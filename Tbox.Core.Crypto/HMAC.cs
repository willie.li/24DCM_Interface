﻿using System.Security.Cryptography;
using System.Text;

namespace Tbox.Core.Crypto;

public static class HMAC
{
    public static string GetHMacmd5Hash(string plainText, string key = "")
    {
        using var hmacmd5 = new HMACMD5();

        if (!string.IsNullOrEmpty(key))
            hmacmd5.Key = Encoding.UTF8.GetBytes(key);

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        var bytes = hmacmd5.ComputeHash(plainTextBytes, 0, plainTextBytes.Length);
        return Convert.ToHexString(bytes);
    }

    public static string GetHMacsha1Hash(string plainText, string key = "")
    {
        using var hmacsha1 = new HMACSHA1();

        if (!string.IsNullOrEmpty(key))
            hmacsha1.Key = Encoding.UTF8.GetBytes(key);

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        var bytes = hmacsha1.ComputeHash(plainTextBytes, 0, plainTextBytes.Length);
        return Convert.ToHexString(bytes);
    }

    public static string GetHMacsha256Hash(string plainText, string key = "")
    {
        using var hmacsha256 = new HMACSHA256();

        if (!string.IsNullOrEmpty(key))
            hmacsha256.Key = Encoding.UTF8.GetBytes(key);

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        var bytes = hmacsha256.ComputeHash(plainTextBytes, 0, plainTextBytes.Length);
        return Convert.ToHexString(bytes);
    }

    public static string GetHMacsha384Hash(string plainText, string key = "")
    {
        using var hmacsha384 = new HMACSHA384();

        if (!string.IsNullOrEmpty(key))
            hmacsha384.Key = Encoding.UTF8.GetBytes(key);

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        var bytes = hmacsha384.ComputeHash(plainTextBytes, 0, plainTextBytes.Length);
        return Convert.ToHexString(bytes);
    }

    public static string GetHMacsha512Hash(string plainText, string key = "")
    {
        using var hmacsha512 = new HMACSHA512();

        if (!string.IsNullOrEmpty(key))
            hmacsha512.Key = Encoding.UTF8.GetBytes(key);

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        var bytes = hmacsha512.ComputeHash(plainTextBytes, 0, plainTextBytes.Length);
        return Convert.ToHexString(bytes);
    }
}