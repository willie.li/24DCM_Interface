﻿using System.Security.Cryptography;
using System.Text;

namespace Tbox.Core.Crypto;

public class AES : IDisposable
{
    private readonly Aes _aes;
    private bool _disposedValue;

    public AES(string key, string iv)
    {
        // AES supports 128, 192, and 256 bits key sizes and 128 bits block size.
        _aes = Aes.Create();

        _aes.KeySize = 256; // key length: 256/8=32
        _aes.Key = Encoding.UTF8.GetBytes(key);

        _aes.BlockSize = 128; // iv length: 128/8=16
        _aes.IV = Encoding.UTF8.GetBytes(iv);

        _aes.Padding = PaddingMode.Zeros;
        _aes.Mode = CipherMode.CBC;
    }

    public string Encrypt(string plainText)
    {
        if (string.IsNullOrEmpty(plainText))
            throw new ArgumentNullException(nameof(plainText));

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        using var encryptor = _aes.CreateEncryptor(_aes.Key, _aes.IV);
        var bytes = encryptor.TransformFinalBlock(plainTextBytes, 0, plainTextBytes.Length);
        //return Convert.ToBase64String(outputBytes);
        return Convert.ToHexString(bytes);
    }

    public string Decrypt(string? source)
    {
        if (string.IsNullOrEmpty(source))
            throw new ArgumentNullException(nameof(source));

        var srcBytes = Convert.FromHexString(source);

        using var decryptor = _aes.CreateDecryptor(_aes.Key, _aes.IV);
        var bytes = decryptor.TransformFinalBlock(srcBytes, 0, srcBytes.Length);
        return Encoding.UTF8.GetString(bytes);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                _aes.Dispose();
            }
            _disposedValue = true;
        }
    }

    ~AES()
    {
        Dispose(disposing: false);
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}