﻿using Tbox.Core.Services;

namespace Tbox.Api.Plugins;

public sealed class GetCarNameEndpointPlugin : IGetCarNameEndpointPlugin
{
    private readonly IDataAccessService _dataAccessService;

    public GetCarNameEndpointPlugin(IDataAccessService dataAccessService)
    {
        _dataAccessService = dataAccessService;
    }

    public (string, string, bool, string?) GetResult(string vin, string oneId)
    {
        if (string.IsNullOrEmpty(vin) || string.IsNullOrEmpty(oneId))
            return (string.Empty, string.Empty, false, string.Empty);

        var (carName, tosSTS, isCarOwner, tosOneId) = _dataAccessService.VehicleInfoAs.GetCarName(vin, oneId);
        return (carName.Replace(" ", ""), tosSTS, isCarOwner, tosOneId);
    }
}

public interface IGetCarNameEndpointPlugin : IPlugin
{
    (string, string, bool, string?) GetResult(string vin, string oneId);
}

public interface IPlugin
{
}