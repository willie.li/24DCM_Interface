﻿using Microsoft.Extensions.Logging;
using System.Text;

namespace Tbox.Core.EncryptedAppSettings;

public class EncryptedAppSettings<T> : IEncryptedAppSettings<T>
{
    private readonly ILogger<EncryptedAppSettings<T>> _logger;
    const string KeyFileName = "ushouldntseeme.data";
    //const string EncryptedAppSettingsFileName = "classified-plaintext.data";
    const string EncryptedAppSettingsFileName = "classified.data";

#if DEBUG
    private readonly int UNLOAD_TIMEOUT_MINUTES = 3;
#else
    private readonly int UNLOAD_TIMEOUT_MINUTES = 30;
#endif

    private readonly IDictionary<int, (string key, string iv)> KEYS = new Dictionary<int, (string key, string iv)>();

    public EncryptedAppSettings(ILogger<EncryptedAppSettings<T>> logger)
    {
        _logger = logger;
        LoadKeys();
    }

    private T? _appSettings = default(T);
    private DateTime _appSettingsExpireTime = DateTime.Now.AddHours(-1);
    private Timer? _innerTimer;
    private bool _disposedValue;

    public T? AppSettings
    {
        get
        {
            _logger.Log(LogLevel.Information, "AppSettings getter called.");
            if (_appSettings == null || ForceReload)
            {
                LoadAppSettings();
            }
            _logger.Log(LogLevel.Information, $"AppSettings:{_appSettings}");
            return _appSettings;
        }
    }

    public bool ForceReload { get; set; } = false;

    public bool ValidContext(string inputContext)
    {
        try
        {
            var inp = inputContext;
            foreach (var k in KEYS.OrderByDescending(o => o.Key))
            {
                inp = TripleDESDecrypt(inp, k.Value.key, k.Value.iv);
            }

            var j = System.Text.Json.JsonSerializer.Deserialize<T>(inp);
            return j is not null;
        }
        catch
        {
            return false;
        }
    }

    void LoadAppSettings()
    {
        var t1 = DateTime.Now;
        var data = ReadFile(EncryptedAppSettingsFileName);
        if (string.IsNullOrEmpty(data))
        {
            throw new Exception("missing appsettings file.");
        }

        _logger.Log(LogLevel.Information, $"Encryped AppSettings data load from file, in {DateTime.Now.Subtract(t1).TotalNanoseconds:#,###} ns., at {DateTime.Now:yyyy-MM-dd HH:mm:ss.fffffff}");

        //foreach (var k in KEYS)
        //{
        //    data = TripleDESEncrypt(data, k.Value.key, k.Value.iv);
        //}

        try
        {
            var de = data;
            foreach (var k in KEYS.OrderByDescending(o => o.Key))
            {
                de = TripleDESDecrypt(de, k.Value.key, k.Value.iv);
            }

            var settings = System.Text.Json.JsonSerializer.Deserialize<T>(de);
            //RC.MemoryCache.Default["SqlConnStrFromKV"] = settings?.DefaultConnection??string.Empty;


            var now = DateTime.Now;
            _appSettingsExpireTime = now.AddSeconds(UNLOAD_TIMEOUT_MINUTES * 60);

            // _innerTime initiate，stop 1000 ms（1 sec），every 1000 ms（1 sec） tick event
            _innerTimer = new Timer(new TimerCallback(TickTimer), null, 1_000, 1_000);

            _appSettings = settings;

#if DEBUG
            _logger.Log(LogLevel.Trace, $"AppSettings: {settings} loaded, in {DateTime.Now.Subtract(t1).TotalNanoseconds:#,###} ns., at {DateTime.Now:yyyy-MM-dd HH:mm:ss.fffffff}");
#endif
        }
        catch
        {
            throw;
        }
    }

    void TickTimer(object state)
    {
        if ((DateTime.Now - _appSettingsExpireTime).Ticks > 0 && _appSettings is not null)
        {
            _appSettings = default;

            if (_innerTimer != null)
            {
                _innerTimer.Change(Timeout.Infinite, Timeout.Infinite);
                _innerTimer.Dispose();
                _innerTimer = null;
            }
            _logger.Log(LogLevel.Information, $"Unload AppSettings, at {DateTime.Now:yyyy/MM/dd HH:mm:ss.fffffff}");
        }
    }

    static string ReadFile(string filename)
    {
        try
        {
            var fpath = Path.Combine(Directory.GetCurrentDirectory(), filename);
            if (File.Exists(fpath))
            {
                using var streamReader = new StreamReader(fpath);
                var data = streamReader.ReadToEnd();
                return data;
            }
            return string.Empty;
        }
        catch
        {
            throw;
        }
    }

    static string TripleDESDecrypt(string encryptedData, string key, string iv)
    {
        using var _3des = new Crypto.TripleDes(key, iv);
        return _3des.Decrypt(encryptedData);
    }

    static string TripleDESEncrypt(string data, string key, string iv)
    {
        using var _3des = new Crypto.TripleDes(key, iv);
        return _3des.Encrypt(data);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                _appSettings = default;
                _appSettingsExpireTime = DateTime.MinValue;
                if (_innerTimer is not null)
                {
                    _innerTimer.Dispose();
                    _innerTimer = null;
                }
            }
            _disposedValue = true;
        }
    }

    ~EncryptedAppSettings()
    {
        Dispose(disposing: false);
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    void LoadKeys()
    {
        try
        {
            var b64 = Encoding.UTF8.GetString(Convert.FromBase64String(ReadFile(KeyFileName)));
            var keys = b64?.Split(Environment.NewLine);
            if (keys?.Any() == true)
            {
                for (var i = 0; i < keys.Length; i++)
                {
                    var k = keys[i];
                    if (k?.Length == 32)
                    {
                        var key = k[..24];
                        var iv = k[24..];
                        if (k?.Any() == true)
                        {
                            KEYS.Add(i, (key, iv));
                        }
                    }
                }
                KEYS.AsReadOnly();
            }
        }
        catch
        {
            throw;
        }
    }
}

public interface IEncryptedAppSettings<T> : IDisposable
{
    T AppSettings { get; }
    bool ForceReload { get; set; }
    public bool ValidContext(string inputContext);
}