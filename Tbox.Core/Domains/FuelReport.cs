﻿namespace Tbox.Core.Domains;

public class FuelReport
{
    [Key]
    public int Id { get; set; }
    [Unicode(false)]
    [MaxLength(50)]
    public string? VIN { get; set; }
    public long Epoch { get; set; }
    /// <summary>
    /// 平均油耗
    /// </summary>
    public double AvgFuelConsum { get; set; }
    /// <summary>
    /// 消耗油量
    /// </summary>
    public double FuelConsumption { get; set; }
    /// <summary>
    /// 行駛距離
    /// </summary>
    public double TravelDistance { get; set; }
    /// <summary>
    /// 加油量
    /// </summary>
    public int FuelDiff { get; set; }
    /// <summary>
    /// 緯度
    /// </summary>
    public double Lat { get; set; }
    /// <summary>
    /// 經度
    /// </summary>
    public double Lon { get; set; }
    /// <summary>
    /// deleted ?
    /// </summary>
    public bool? IsDeleted { get; set; }
    /// <summary>
    /// 使用者編輯：當次加油金額
    /// </summary>
    public decimal? UserAmount { get; set; }
    /// <summary>
    /// 使用者編輯：當次加油量
    /// </summary>
    public int? UserFuelDiff { get; set; }
    /// <summary>
    /// guid key
    /// </summary>
    [Unicode(false)]
    [MaxLength(36)]
    public string? PermanentId { get; set; }

    public DateTime? ModifiedTime { get; set; }
    public DateTime? CreatedTime { get; set; }
}