﻿namespace Tbox.Core.Domains;

public class FuelSetting
{
    [Key]
    public int Id { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? VIN { get; set; }
    [Unicode(false)]
    [MaxLength(36)]
    public string? OneId { get; set; }
    /// <summary>
    /// from OilType enum
    /// </summary>
    public int OilType { get; set; }
    public DateTime ModifiedTime { get; set; }
}