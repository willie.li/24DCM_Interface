﻿using System;

namespace Tbox.Core.Domains;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<VehicleInfoA>().ToTable("VehicleInfoA");
        modelBuilder.Entity<VehicleInfoB2>().ToTable("VehicleInfoB2");
        modelBuilder.Entity<VehicleInfoB2>().HasKey(e => new { e.VIN, e.ONEID });
        modelBuilder.Entity<VehicleVinRef>().ToTable("VehicleVinRef");
        modelBuilder.Entity<VehicleVinRef>().HasKey(e => e.OBD2VIN);
        modelBuilder.Entity<VehicleVinRef>().HasIndex(e => e.OBD2VIN).IsUnique(true);
        modelBuilder.Entity<TBoxNotificationSetting>().HasKey(e => new { e.VIN, e.OneId, e.Dxid });
        modelBuilder.Entity<FuelPricing>(e =>
        {
            e.Property(p => p.Oil92).HasColumnType("DECIMAL(5, 2)");
            e.Property(p => p.Oil95).HasColumnType("DECIMAL(5, 2)");
            e.Property(p => p.Oil98).HasColumnType("DECIMAL(5, 2)");
            e.Property(p => p.Diesel).HasColumnType("DECIMAL(5, 2)");
        });
        modelBuilder.Entity<FuelReport>(e => e.Property(p => p.UserAmount).HasColumnType("DECIMAL(8, 2)"));
        modelBuilder.Entity<VehicleInfoB2Demo>().ToTable("VehicleInfoB2_DEMO");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            
            var connStr = Strings.FindSqlString(AppDomain.CurrentDomain);
            optionsBuilder.UseSqlServer(connStr, sqlServerOptionsAction =>
            {
                sqlServerOptionsAction.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(1), errorNumbersToAdd: Array.Empty<int>());
                sqlServerOptionsAction.MigrationsAssembly("Tbox.Api");
            });

#if DEBUG
            optionsBuilder.EnableDetailedErrors();
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.ConfigureWarnings(warningsAction =>
            {
                warningsAction.Log(new EventId[] {
                    CoreEventId.FirstWithoutOrderByAndFilterWarning,
                    CoreEventId.RowLimitingOperationWithoutOrderByWarning
                });
            });
#endif
        }
    }

    public DbSet<VehicleInfoA> VehicleInfoAs { get; set; }
    public DbSet<VehicleInfoB2> VehicleInfoB2s { get; set; }
    public DbSet<VehicleVinRef> VehicleVinRefs { get; set; }
    public DbSet<TBoxNotificationSetting> TBoxNotificationSettings { get; set; }
    public DbSet<FuelPricing> FuelPricings { get; set; }
    public DbSet<FuelSetting> FuelSettings { get; set; }
    public DbSet<FuelReport> FuelReports { get; set; }
    public DbSet<FcmInfo> FcmInfos { get; set; }
    public DbSet<VehicleInfoB2Demo> VehicleInfoB2Demos { get; set; }
}