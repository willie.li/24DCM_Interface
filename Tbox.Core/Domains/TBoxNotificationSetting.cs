﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tbox.Core.Domains;

public class TBoxNotificationSetting
{
    [Unicode(false)]
    [MaxLength(50)]
    public string? OneId { get; set; }
    [Unicode(false)]
    [MaxLength(100)]
    public string? Dxid { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? VIN { get; set; }
    public bool StartupNotification { get; set; }
    public bool LowBatteryNotification { get; set; }
    public bool StopWithoutLockNotification { get; set; }
    public bool AntiThieftFailureNotification { get; set; }
    public bool MovingFailureNotification { get; set; }
    public DateTime CreatedTime { get; set; }
    public DateTime MdifiedTime { get; set; }
}