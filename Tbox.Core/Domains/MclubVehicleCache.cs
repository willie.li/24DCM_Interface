﻿namespace Tbox.Core.Domains;

/// <summary>
/// Cache 資料表
/// </summary>
public class MclubVehicleCache
{
    [Unicode(false)]
    [MaxLength(20)]
    public string? VIN { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? LICSNO { get; set; }
    [Unicode(false)]
    [MaxLength(10)]
    public string? REDLDT { get; set; }
    public DateTime REDLDT_r { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? STSCD { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? WHBRNHNM { get; set; }
    public DateTime CRTDT { get; set; }
    [Unicode(false)]
    [MaxLength(100)]
    public string? CRTPGMID { get; set; }
    public DateTime MTDT { get; set; }
    [Unicode(false)]
    [MaxLength(100)]
    public string? MTPGMID { get; set; }
}