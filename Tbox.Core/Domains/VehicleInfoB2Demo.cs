﻿namespace Tbox.Core.Domains;

public sealed class VehicleInfoB2Demo
{
    [Key]
    [Unicode(false)]
    [MaxLength(20)]
    public string? VIN { get; set; }
}