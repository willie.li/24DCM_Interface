﻿namespace Tbox.Core.Domains;

public sealed class VehicleInfoB2
{
    [Key]
    [Unicode(false)]
    [MaxLength(20)]
    public string? VIN { get; set; }
    [Unicode(false)]
    public Guid ONEID { get; set; }
    [Unicode(false)]
    [MaxLength(1)]
    public string? ONEIDSTS { get; set; }
    [Unicode(false)]
    [MaxLength(1)]
    public string? TOSSTS { get; set; }
    [Unicode(false)]
    [MaxLength(10)]
    public string? TOSVER { get; set; }
    public DateTime CRTDT { get; set; }
    [Unicode(false)]
    [MaxLength(45)]
    public string? CRTPGMID { get; set; }
    public DateTime MTDT { get; set; }
    [Unicode(false)]
    [MaxLength(45)]
    public string? MTPGMID { get; set; }
    [Unicode(false)]
    [MaxLength(10)]
    public string? LICSNO { get; set; }
    public DateTime? SetAgreeTime { get; set; }
}