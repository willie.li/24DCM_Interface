﻿namespace Tbox.Core.Domains;

public class VehicleVinRef
{
    /// <summary>
    /// Unique PK
    /// </summary>
    [Key]
    [Unicode(false)]
    [MaxLength(30)]
    public string OBD2VIN { get; set; } = string.Empty;
    [Unicode(false)]
    [MaxLength(20)]
    public string VIN { get; set; } = string.Empty;
    public DateTime CRTDT { get; set; }
    [MaxLength(45)]
    public string? CRTPGMID { get; set; }
    public DateTime MTDT { get; set; }
    [MaxLength(45)]
    public string? MTPGMID { get; set; }
}