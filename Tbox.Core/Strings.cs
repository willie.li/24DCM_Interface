﻿using Microsoft.AspNetCore.Http;
using RC = System.Runtime.Caching;

namespace Tbox.Core;

public static class Strings
{
    public static string FindSqlString(this AppDomain appDomain)
    {
        //var tboxSqlStr = Convert.ToString(RC.MemoryCache.Default[Constants.TboxSqlConnectionStringKeyName]);
        try
        {
        var tboxSqlStr = appDomain.GetData(Constants.TboxSqlConnectionStringKeyName).ToString();
            return tboxSqlStr ?? string.Empty;
        }
        catch(Exception ex) { throw ex.InnerException; };
    }

    public static string TryGetClientIP(IHttpContextAccessor httpContextAccessor)
    {
        if (httpContextAccessor is not null)
        {
            var headers = httpContextAccessor.HttpContext?.Request.Headers;
            if (headers?.Any() == true)
            {
                var clientIP = headers["X-Azure-ClientIP"].FirstOrDefault() ?? headers["X-Azure-SocketIP"].FirstOrDefault() ?? string.Empty;
                return clientIP;
            }
        }
        return string.Empty;
    }

    public static async IAsyncEnumerable<string> ReadLogPerLing(string filepath)
    {
        var logFile = new FileInfo(Path.Combine(filepath));
        if (logFile.Exists)
        {
            using var streamReader = new StreamReader(filepath, new FileStreamOptions
            {
                Access = FileAccess.Read,
                Mode = FileMode.Open,
                Share = FileShare.ReadWrite
            });
            while (!streamReader.EndOfStream)
            {
                var l = await streamReader.ReadLineAsync();
                yield return l ?? string.Empty;
            }
        }
    }

    public static bool CheckAccessable(IHttpContextAccessor httpContextAccessor, ILogger logger)
    {
        var accessableIPs = new[] { "61.216.190.109", "::1" };
        var fdClientId = httpContextAccessor.HttpContext.Request.Headers["X-Azure-ClientIP"].FirstOrDefault();
        if (!string.IsNullOrEmpty(fdClientId))
        {
            logger.Log(LogLevel.Information, $"{nameof(Strings)} | {nameof(CheckAccessable)} | fdClientId => {fdClientId}");

            if (!accessableIPs.Contains(fdClientId))
                return false;
        }
        else
        {
            var thisIp = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            logger.Log(LogLevel.Information, $"{nameof(Strings)} | {nameof(CheckAccessable)} | thisIp => {thisIp}");

            if (!accessableIPs.Contains(thisIp))
                return false;
        }
        return true;
    }

    public static bool IsLocalhost(string host)
    {
        if (host.Equals("localhost", StringComparison.OrdinalIgnoreCase) || host.Equals("127.0.0.1", StringComparison.OrdinalIgnoreCase))
        {
            return true;
        }
        return false;
    }
}