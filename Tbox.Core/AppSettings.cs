﻿namespace Tbox.Core;

#nullable disable
public sealed record AppSettings
{
    public string TboxDbConnection { get; init; }
    public string MclubDbConnection { get; init; }
    public string BasicUrl { get; init; }
    public bool IsBypassTokenValidation { get; init; }
    public string CmxEntryDomain { get; init; }
    public string AppEntryDomain { get; init; }
    public string DevEntryDomain { get; init; }
    public string McServiceUrl { get; init; }
    public string McAppId { get; init; }
    public string McAppVersion { get; init; }
    public bool IsCallingMCTokenCheck { get; init; }
    public bool IsTestAppMode { get; init; }
    public string Htapp20ServiceUrl { get; init; }
    public TokenValidator TokenValidator { get; init; }
}

public sealed record TokenValidator
{
    public string Sub { get; init; }
    public string Jti { get; init; }
    public string Iss { get; init; }
    public string PublicKey { get; init; }
}