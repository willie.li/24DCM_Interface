﻿using System.Text;
using System.Text.Json;
using Tbox.Core.Models.RequestModels;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX11:開啟借車模式
    /// </summary>
    /// <param name="vin"></param>
    /// <param name="req"></param>
    /// <returns></returns>
    public async Task<(bool status, string results)> StartLendingMode(string vin, StartLendingModeRequestModel req)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri($"{_baseUrl}/Start_LendingMode/{vin}"),
            Content = new StringContent(JsonSerializer.Serialize(req), Encoding.UTF8, "application/json")
        };
        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                return (true, string.Empty);
            }
            else
            {
                if (resp is not null)
                {
                    var content = await resp.Content.ReadAsStringAsync();
                    return (false, content);
                }
                return (false, "Something went wrong.");
            }
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(StartLendingMode)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return (false, ex.Message);
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<(bool status, string results)> StartLendingMode(string vin, StartLendingModeRequestModel req);
}