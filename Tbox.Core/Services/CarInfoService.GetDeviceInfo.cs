﻿using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    public async Task<DeviceInfoModel?> GetDeviceInfo(string vin)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_Device_Info/{vin}")
        };
        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = JsonSerializer.Deserialize<DeviceInfoModel?>(content);
                    if (data is not null)
                    {
                        return data;
                    }
                }
            }
            return new DeviceInfoModel();
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetDeviceInfo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return null;
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<DeviceInfoModel?> GetDeviceInfo(string vin);
}