﻿using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    public async Task<DriveReportModel?> GetLendingDriveReport(string vin, long epochstart, long epochend)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_LendingDriveReport_List/{vin}?epochstart={epochstart}&epochend={epochend}")
        };
        try
        {
            _httpClient.Send(Request);

            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = System.Text.Json.JsonSerializer.Deserialize<DriveReportModel>(content);
                    if (data is not null)
                    {
                        return data;
                    }
                }
            }
            return new DriveReportModel
            {
                Total = 0,
                Reports = null
            };
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetLendingDriveReport)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return new DriveReportModel
            {
                Total = 0,
                Reports = null
            };
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<DriveReportModel?> GetLendingDriveReport(string vin, long epochstart, long epochend);
}