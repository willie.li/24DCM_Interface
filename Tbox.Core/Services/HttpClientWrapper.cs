﻿using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Tbox.Core.Services;

public sealed class HttpClientWrapper : IHttpClientWrapper
{
    private readonly ILogger<HttpClientWrapper> _logger;
    private readonly HttpClient _httpClient;

    public HttpClientWrapper(ILogger<HttpClientWrapper> logger)
    {
        _logger = logger;
        _httpClient = new HttpClient(new SocketsHttpHandler
        {
            PooledConnectionLifetime = TimeSpan.FromMinutes(10),
            PooledConnectionIdleTimeout = TimeSpan.FromMinutes(5),
            MaxConnectionsPerServer = int.MaxValue
        })
        {
            Timeout = TimeSpan.FromMinutes(10),
            DefaultRequestVersion = new Version(2, 0)
        };
        _httpClient.DefaultRequestHeaders.Clear();

        HttpClientInstance = _httpClient;
    }

    /// <summary>
    /// a httpClient instance
    /// </summary>
    public HttpClient HttpClientInstance { get; private set; }
    /// <summary>
    /// is write to log, default to true
    /// </summary>
    public bool IsWriteLog { get; set; } = true;
    /// <summary>
    /// a Http Requet, MUST set before Send()
    /// </summary>
    //public HttpRequestMessage? Request { get; set; }
    /// <summary>
    /// a Http Response, CAN get after Send()
    /// </summary>
    public HttpResponseMessage? Response { get; private set; }
    /// <summary>
    /// a content inside response
    /// </summary>
    public string Content { get; private set; } = string.Empty;

    /// <summary>
    /// a cancellation token
    /// </summary>
    public CancellationToken CancellationToken { get; set; } = default;

    /// <summary>
    /// the execution of Http Send
    /// </summary>
    /// <returns></returns>
    public void Send(HttpRequestMessage? Request)
    {
        if (Request is not null)
        {
            try
            {
                if (IsWriteLog)
                {
                    var jso = new JsonSerializerOptions
                    {
                        Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.CjkUnifiedIdeographs)
                    };
                    _logger.LogInformation($"{nameof(HttpClientWrapper)} | {nameof(Send)} | {nameof(Request)}\n=> {JsonSerializer.Serialize(Request, jso)}");
                    if (Request.Content is not null)
                    {
                        var content = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        _logger.LogInformation($"{nameof(HttpClientWrapper)} | {nameof(Send)} | {nameof(Request)} {nameof(Request.Content)} \n=> {JsonSerializer.Serialize(content)}");
                    }
                }

                //var resp = await _httpClient.SendAsync(Request, CancellationToken);
                //Response = resp;
                //Request = null;

                var respTask = _httpClient.SendAsync(Request, CancellationToken);
                Task.WaitAny(respTask);

                if (respTask.IsCompletedSuccessfully)
                {
                    var resp = respTask.Result;
                    Response = resp;

                    Request = null;

                    if (Response is null)
                    {
                        _logger.LogError($"{nameof(HttpClientWrapper)} | {nameof(Send)} | Response Task.IsCompletedSuccessfully | null response occurred!");
                    }
                    else
                    {
                        var contentTask = resp.Content.ReadAsStringAsync();
                        Task.WaitAny(contentTask);

                        var content = contentTask.Result;
                        Content = content;

                        if (IsWriteLog)
                            _logger.LogInformation($"{nameof(HttpClientWrapper)} | {nameof(Send)} | Response Task.IsCompletedSuccessfully | {nameof(Response)} {nameof(content)}\n=> {content}");
                    }
                }
                else
                {
                    _logger.LogError($"{nameof(HttpClientWrapper)} | {nameof(Send)} | Response status: {respTask.Status}!");
                }
            }
            catch (HttpRequestException ex)
            {
#if DEBUG
                throw;
#else
				_logger.LogError($"{nameof(HttpClientWrapper)} | {nameof(Send)} | {nameof(HttpRequestException)} | {ex.Message}\n{ex.InnerException}\n{ex.StackTrace}");
#endif
            }
            catch (TimeoutException ex)
            {
#if DEBUG
                throw;
#else
                _logger.LogError($"{nameof(HttpClientWrapper)} | {nameof(Send)} | {nameof(TimeoutException)} | {ex.Message}\n{ex.InnerException}\n{ex.StackTrace}");
#endif
            }
            catch (OperationCanceledException ex)
            {
#if DEBUG
                throw;
#else
                _logger.LogError($"{nameof(HttpClientWrapper)} | {nameof(Send)} | {nameof(OperationCanceledException)} | {ex.Message}\n{ex.InnerException}\n{ex.StackTrace}");
#endif
            }
            catch (Exception ex)
            {
#if DEBUG
                throw;
#else
				_logger.LogError($"{nameof(HttpClientWrapper)} | {nameof(Send)} | {ex.Message}\n{ex.InnerException}\n{ex.StackTrace}");
#endif
            }
        }
    }
}

public interface IHttpClientWrapper
{
    HttpClient HttpClientInstance { get; }
    bool IsWriteLog { get; set; }
    //HttpRequestMessage? Request { get; set; }
    void Send(HttpRequestMessage? Request);
    HttpResponseMessage? Response { get; }
    string Content { get; }
    CancellationToken CancellationToken { get; set; }
}