﻿using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX03:查詢車輛行駛報告列表 api caller
    /// </summary>
    /// <param name="vin">引擎號碼</param>
    /// <param name="epochstart">查詢日期區間 起</param>
    /// <param name="epochend">查詢日期區間 迄</param>
    /// <returns>DriverReport</returns>
    public async Task<DriveReportModel?> GetDriveReportList(string vin, long epochstart, long epochend)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_DriveReport_List/{vin}?epochstart={epochstart}&epochend={epochend}"),
        };

        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = JsonSerializer.Deserialize<DriveReportModel>(content);
                    if (data is not null)
                    {
                        return data;
                    }
                }
            }
            return new DriveReportModel
            {
                Total = 0,
                Reports = null
            };
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetDriveReportList)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return new DriveReportModel
            {
                Total = 0,
                Reports = null
            };
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<DriveReportModel?> GetDriveReportList(string vin, long epochstart, long epochend);
}