﻿using Azure.Core;
using Tbox.Core.EncryptedAppSettings;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService : ICarInfoService
{
    private readonly ILogger<CarInfoService> _logger;
    private readonly IHttpClientWrapper _httpClient;
    private readonly string? _baseUrl;

    public CarInfoService(ILogger<CarInfoService> logger, IHttpClientWrapper httpClient, IEncryptedAppSettings<AppSettings> appSettings)
    {
        _logger = logger;
        _httpClient = httpClient;
        _baseUrl = appSettings.AppSettings.BasicUrl;
    }

    /// <summary>
    /// CMX01:查詢車輛GPS位置 api caller
    /// </summary>
    /// <param name="vin">引擎號碼</param>
    /// <returns>GpsInfo</returns>
    public async Task<GpsInfoModel?> GetCarGps(string vin)
    {
        var req = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_GPS_Info/{vin}")
        };

        try
        {
            var Request = req;
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var json = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(json))
                {
                    var data = System.Text.Json.JsonSerializer.Deserialize<GpsInfoModel>(json);
                    if (data is not null)
                    {
                        if (data.VehicleStatus is >= 1 and <= 2)
                        {
                            data.VehicleStatusName = ((VehicleStatusEnum)data.VehicleStatus).ToString();
                        }
                        else
                        {
                            data.VehicleStatusName = "異常";
                        }

                        if (data.GpsStatus >= 1 && data.GpsStatus <= 2)
                        {
                            data.GpsStatusName = ((GpsStatusEnum)data.GpsStatus).ToString();
                        }
                        else
                        {
                            data.GpsStatusName = "未定位";
                        }
                        return data;
                    }
                }
            }
            return null;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetCarGps)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return null;
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<GpsInfoModel?> GetCarGps(string vin);
}