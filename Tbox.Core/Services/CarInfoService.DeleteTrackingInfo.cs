﻿#if RELEASE
using Microsoft.Extensions.Logging;
#endif


using Azure.Core;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX06-2:刪除車輛異常移位內容
    /// </summary>
    /// <param name="vin"></param>
    /// <param name="trackId"></param>
    public async Task DeleteTrackingInfo(string vin, long trackId)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Delete,
            RequestUri = new Uri($"{_baseUrl}/Del_Tracking_Info?vin={vin}&trackid={trackId}")
        };
        try
        {
            _httpClient.Send(Request);
            await Task.CompletedTask;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(DeleteTrackingInfo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            await Task.CompletedTask;
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task DeleteTrackingInfo(string vin, long trackId);
}