﻿#if RELEASE
using Microsoft.Extensions.Logging;
#endif
using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX04:查詢車輛行駛報告內容 api caller
    /// </summary>
    /// <param name="trackId">TrackId</param>
    /// <returns>IEnumerable<GpsInfo></returns>
    public async Task<IEnumerable<GpsInfoModel>?> GetDriveReportInfo(string vin, long trackId)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_DriveReport_Info/{vin}/{trackId}")
        };

        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = JsonSerializer.Deserialize<IEnumerable<GpsInfoModel>>(content);
                    if (data?.Any() == true)
                    {
                        foreach (var item in data)
                        {
                            item.VehicleStatusName = ((VehicleStatusEnum)item.VehicleStatus).ToString();
                            item.GpsStatusName = ((GpsStatusEnum)item.GpsStatus).ToString();
                        }
                        return data;
                    }
                }
            }
            return Array.Empty<GpsInfoModel>();
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetDriveReportInfo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return Array.Empty<GpsInfoModel>();
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<IEnumerable<GpsInfoModel>?> GetDriveReportInfo(string vin, long trackId);
}