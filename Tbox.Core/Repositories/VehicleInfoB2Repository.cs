﻿using Dapper;
using Microsoft.Data.SqlClient;
using Tbox.Core.Domains;
using Tbox.Core.Models;
using Tbox.Core.Models.RequestModels;

namespace Tbox.Core.Repositories;

public sealed class VehicleInfoB2Repository : Repository<VehicleInfoB2>, IVehicleInfoB2Repository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public VehicleInfoB2Repository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<VehicleInfoB2?> Get([MaxLength(20)] string vin)
    {
        var b2 = await _db.VehicleInfoB2s.FirstOrDefaultAsync(o => o.TOSSTS == "Y" && o.VIN == vin);
        return b2;
    }

    public async Task<VehicleTermModel?> SetVersionAgreement(SetVersionAgreementRequestModel data)
    {
        if (data is null)
            return null;

        // 同 VIN，可以擁有多筆 oneid，但是一個 VIN 永遠只能有一個同意條款為 Y。
        // 設定同意條款時，
        // 1. 若 VIN 已有同意條款為 'Y'，不論 Oneid 則無法再設定
        // 2. 再由當次 request，設定當時同意的 VIN-OneId 為 'Y'

        var sql = "";
        var tdStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        _ = bool.TryParse(Convert.ToString(data.IsAgree ?? ""), out bool b);
        var bguid = Guid.TryParse(data.OneId ?? "", out Guid gONEID);
        if (b && bguid)
        {
            sql = $@"DECLARE @vy int = (SELECT COUNT(1) FROM [VehicleInfoB2] WHERE [VIN] = @VIN AND [TOSSTS] = 'Y');
IF @vy = 0
  BEGIN
    DECLARE @ct int = (SELECT COUNT(1) FROM [VehicleInfoB2] WHERE [VIN]=@VIN AND [ONEID]=@OneId);
    IF @ct = 0
        BEGIN
            INSERT INTO [VehicleInfoB2] ([VIN], [ONEID], [ONEIDSTS], [TOSSTS], [TOSVER], [CUSTID], [CRTDT], [CRTPGMID], [MTDT], [MTPGMID], [LICSNO], [SetAgreeTime])
            VALUES(@VIN, @OneId, 'Y', 'Y', @VersionAgreed, @CustId, GETDATE(), @ip, GETDATE(), @ip, @LICSNO, GETDATE());
        END
    ELSE
        BEGIN
            UPDATE [VehicleInfoB2]
            SET [TOSSTS]='Y', [TOSVER]=@VersionAgreed, [CUSTID]=@CustId, [MTPGMID]=@ip, [MTDT]='{tdStr}', [LICSNO]=@LICSNO, [SetAgreeTime]='{tdStr}'
            WHERE [VIN]=@VIN AND [ONEID]=@OneId
        END
  END


SELECT [VIN]
      ,CONVERT(Nvarchar(36), [ONEID]) AS [ONEID]
      ,[ONEIDSTS]
      ,[TOSSTS]
      ,[TOSVER]
      ,[CUSTID] FROM [VehicleInfoB2] WHERE [VIN] = @VIN AND [ONEID] = @OneId;";
        }
        else
        {
            return null;
        }

        try
        {
            var conn = (SqlConnection)_db.Database.GetDbConnection();
            if (conn.State != System.Data.ConnectionState.Open)
            {
                await conn.OpenAsync();
            }

            return await conn.QueryFirstOrDefaultAsync<VehicleTermModel>(sql, new
            {
                VIN = new DbString { Value = data.VIN, IsAnsi = true, Length = 20 },
                OneId = new DbString { Value = data.OneId, IsAnsi = true, IsFixedLength = true, Length = 36 },
                CustId = new DbString { Value = data.CustId, IsAnsi = true, IsFixedLength = true, Length = 10 },
                VersionAgreed = new DbString { Value = data.VersionAgreed, IsAnsi = true, IsFixedLength = true, Length = 10 },
                LICSNO = new DbString { Value = data.LICSNO, IsAnsi = true, Length = 20 },
                IP = new DbString { Value = data.IP, IsAnsi = true, Length = 45 }
            });
        }
        catch
        {
#if DEBUG
            throw;
#else
            return null;
#endif
        }
    }

    public async Task<VehicleTermModel?> RevokeVinTOS(string vin, string oneId, string ip)
    {
        if (string.IsNullOrEmpty(vin) || string.IsNullOrEmpty(oneId))
            return null;

        var sql = $@"UPDATE [VehicleInfoB2]
SET [TOSSTS] = 'N', [TOSVER] = '', [MTDT] = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}', [MTPGMID] = @ip
WHERE [VIN] = @VIN AND [ONEID] = @OneId AND [TOSSTS] = 'Y';

SELECT [VIN]
      ,CONVERT(Nvarchar(100), [ONEID]) AS [ONEID]
      ,[ONEIDSTS]
      ,[TOSSTS]
      ,[TOSVER]
      ,[CUSTID] FROM [VehicleInfoB2] WHERE [VIN] = @VIN AND [ONEID] = @OneId;";

        try
        {
            var conn = (SqlConnection)_db.Database.GetDbConnection();
            if (conn.State != System.Data.ConnectionState.Open)
            {
                await conn.OpenAsync();
            }

            var ret = await conn.QueryFirstOrDefaultAsync<VehicleTermModel>(sql, new
            {
                vin = new DbString { Value = vin, IsAnsi = true, Length = 20 },
                oneId = new DbString { Value = oneId, IsAnsi = true, IsFixedLength = true, Length = 36 },
                ip = new DbString { Value = ip, IsAnsi = true, Length = 20 }
            });
            return ret;
        }
        catch
        {
#if DEBUG
            throw;
#else
                return null;
#endif
        }
    }

    /// <summary>
    /// 以 VIN 查找已設定同意條款之時間
    /// </summary>
    /// <returns></returns>
    public async Task<DateTime?> FindAgreedTime(string vin, string oneId)
    {
        _ = Guid.TryParse(oneId, out var gOneId);
        try
        {
            var term = await _db.VehicleInfoB2s.FirstOrDefaultAsync(o => o.VIN == vin && o.ONEID == gOneId && o.TOSSTS == "Y");
            if (term is not null)
            {
                return term?.SetAgreeTime.HasValue == true
                    ? term.SetAgreeTime.Value
                    : term?.MTDT;
            }
            else
            {
                return DateTime.MaxValue;
            }
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleInfoB2Repository)} | {nameof(FindAgreedTime)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return DateTime.MinValue;
#endif
        }
    }

    /// <summary>
    /// 以 VIN 查找已設定同意條款之時間
    /// </summary>
    /// <returns></returns>
    public async Task<DateTime?> FindAgreedTime(string vin)
    {
        try
        {
            var term = await _db.VehicleInfoB2s.FirstOrDefaultAsync(o => o.VIN == vin && o.TOSSTS == "Y");
            if (term is not null)
            {
                return term?.SetAgreeTime.HasValue == true
                    ? term.SetAgreeTime.Value
                    : term?.MTDT;
            }
            else
            {
                return DateTime.MinValue;
            }
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleInfoB2Repository)} | {nameof(FindAgreedTime)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return DateTime.MinValue;
#endif
        }
    }

    /// <summary>
    /// CMX03456 整合查詢使用，Epochstart 重新定義
    /// </summary>
    /// <param name="vin"></param>
    /// <param name="epochstart"></param>
    /// <returns></returns>
    public async Task<long> EpochstartRedefine(string vin, long epochstart)
    {
        try
        {
            var agreedTime = await FindAgreedTime(vin);
            if (agreedTime is not null
                && agreedTime > new DateTime(1, 1, 1))
            {
                var epochY = ((DateTimeOffset)agreedTime).ToUnixTimeSeconds();
                if (epochY > epochstart)
                {
                    epochstart = epochY;
                }
                return epochstart;
            }
            return 0;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleInfoB2Repository)} | {nameof(EpochstartRedefine)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return 0;
#endif
        }
    }

    /// <summary>
    /// 回寫車牌號
    /// </summary>
    /// <param name="vin"></param>
    /// <param name="licsno"></param>
    /// <returns></returns>
    public async Task WriteBackLicenseNo(string vin, string licsno)
    {
        try
        {
            var b2s = _db.VehicleInfoB2s.Where(o => o.VIN == vin);
            if (b2s?.Any() == true)
            {
                foreach (var b2 in b2s)
                {
                    b2.LICSNO = licsno;
                }
                await _db.SaveChangesAsync();
            }
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleInfoB2Repository)} | {nameof(WriteBackLicenseNo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
#endif
        }
    }

    /// <summary>
    /// 2021-11-30：將(TBOX)訊息推播產生時間早於(APP)使用者同意條款時間的推播訊息擋下來不給到firebase
    /// </summary>
    /// <param name="vin">VIN</param>
    /// <returns></returns>
    public async Task<bool> CheckAgreetimeIsBeforeCurrentTime(string vin)
    {
        try
        {
            var b2 = await _db.VehicleInfoB2s.FirstOrDefaultAsync(o => o.TOSSTS == "Y" && o.VIN == vin);
            if (b2 is not null)
            {
                var agreeTime = b2.SetAgreeTime ?? b2.CRTDT;
                var delta = (DateTime.Now - agreeTime).TotalMilliseconds;
                return delta <= 0;
            }
            return false;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleInfoB2Repository)} | {nameof(CheckAgreetimeIsBeforeCurrentTime)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return false;
#endif
        }
    }

    public async Task<bool> CheckIsValidUser(string oneId, string vin)
    {
        try
        {
            _ = Guid.TryParse(oneId, out var gOneId);
            var b2 = await _db.VehicleInfoB2s.FirstOrDefaultAsync(o => o.ONEID == gOneId && o.VIN == vin);
            return b2 is not null;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleInfoB2Repository)} | {nameof(CheckIsValidUser)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return false;
#endif
        }
    }
}

public interface IVehicleInfoB2Repository
{
    Task<VehicleInfoB2?> Get(string vin);
    Task<VehicleTermModel?> SetVersionAgreement(SetVersionAgreementRequestModel data);
    Task<VehicleTermModel?> RevokeVinTOS(string vin, string oneId, string ip);
    Task<DateTime?> FindAgreedTime(string vin, string oneId);
    Task<DateTime?> FindAgreedTime(string vin);
    Task<long> EpochstartRedefine(string vin, long epochstart);
    Task WriteBackLicenseNo(string vin, string licsno);
    Task<bool> CheckAgreetimeIsBeforeCurrentTime(string vin);
    Task<bool> CheckIsValidUser(string oneId, string vin);
}