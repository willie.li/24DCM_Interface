﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Tbox.Core.Domains;

namespace Tbox.Core.Repositories;

public sealed class FuelSettingRepository : Repository<FuelSetting>, IFuelSettingRepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public FuelSettingRepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<int> GetSetting(string vin, string oneId)
    {
        try
        {
            var setting = await _db.FuelSettings.FirstOrDefaultAsync(o => o.VIN == vin && o.OneId == oneId);
            if (setting is not null)
            {
                return setting.OilType;
            }
            return -1;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(FuelSettingRepository)} | {nameof(GetSetting)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return -1;
#endif
        }
    }

    public async Task AddOrUpdate(string vin, string oneId, int fuelType)
    {
        try
        {
            var setting = await _db.FuelSettings.FirstOrDefaultAsync(o => o.VIN == vin && o.OneId == oneId);
            if (setting is null)
            {
                _db.FuelSettings.Add(new FuelSetting
                {
                    VIN = vin,
                    OneId = oneId,
                    OilType = fuelType
                });
            }
            else
            {
                if (fuelType > -1)
                {
                    setting.OilType = fuelType;
                    setting.ModifiedTime = DateTime.Now;
                }
                _db.FuelSettings.Update(setting);
            }
            await _db.SaveChangesAsync();
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(FuelSettingRepository)} | {nameof(AddOrUpdate)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
#endif
        }
    }
}

public interface IFuelSettingRepository
{
    Task<int> GetSetting(string vin, string oneId);
    Task AddOrUpdate(string vin, string oneId, int fuelType);
}