﻿using Microsoft.EntityFrameworkCore;
using Tbox.Core.Domains;

namespace Tbox.Core.Repositories;

public sealed class FuelPriceRepository : Repository<FuelPricing>, IFuelPriceRepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public FuelPriceRepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<(int setting, double price)> GetPriceWithSetting(string vin, string oneId)
    {
        var setting = await _db.FuelSettings.FirstOrDefaultAsync(o => o.VIN == vin && o.OneId == oneId);
        var k = await _db.FuelPricings.FirstOrDefaultAsync(o => o.Oil92 + o.Oil95 + o.Oil98 + o.Diesel > 0);

        if (k is null) return (-1, -1);

        if (setting is null)
            return (-1, (double)k.Oil95); // default to Oil05

        return setting.OilType switch
        {
            0 => (0, (double)k.Oil92),
            2 => (2, (double)k.Oil98),
            3 => (3, (double)k.Diesel),
            _ => (1, (double)k.Oil95),
        };
    }

    public async Task UpdatePrice(double oil92, double oil95, double oil98, double diesel)
    {
        var k = await _db.FuelPricings.FirstOrDefaultAsync(o => o.Id == 1);
        if (k is not null)
        {
            k.Oil92 = (decimal)oil92;
            k.Oil95 = (decimal)oil95;
            k.Oil98 = (decimal)oil98;
            k.ModifiedTime = DateTime.Now;

            await _db.SaveChangesAsync();
        }
    }
}

public interface IFuelPriceRepository
{
    Task<(int setting, double price)> GetPriceWithSetting(string vin, string oneId);
    Task UpdatePrice(double oil92, double oil95, double oil98, double diesel);
}