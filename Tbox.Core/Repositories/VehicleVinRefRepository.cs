﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Tbox.Core.Domains;

namespace Tbox.Core.Repositories;

public sealed class VehicleVinRefRepository : Repository<VehicleVinRef>, IVehicleVinRefRepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public VehicleVinRefRepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public new VehicleVinRef? Add(VehicleVinRef data)
    {
        try
        {
            if (data is not null)
            {
                var ret = _db.VehicleVinRefs.Add(data);
                _db.SaveChanges();

                return ret.Entity;
            }
            return null;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(VehicleVinRefRepository)} | {nameof(Add)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return null;
#endif
        }
        
    }

    public async Task<VehicleVinRef?> Get(string obd2vin)
    {
        //return await _db.VehicleVinRefs.FindAsync(obd2vin);
        var veh = await _db.VehicleVinRefs.FirstOrDefaultAsync(o => o.OBD2VIN == obd2vin);
        return veh;
    }
}

public interface IVehicleVinRefRepository
{
    VehicleVinRef? Add(VehicleVinRef data);
    Task<VehicleVinRef?> Get(string obd2vin);
}