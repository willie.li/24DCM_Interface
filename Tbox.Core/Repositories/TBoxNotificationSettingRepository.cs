﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tbox.Core.Domains;
using Tbox.Core.Models.RequestModels;

namespace Tbox.Core.Repositories;

public sealed class TBoxNotificationSettingRepository : Repository<TBoxNotificationSetting>, ITBoxNotificationSettingRepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public TBoxNotificationSettingRepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<IEnumerable<TBoxNotificationSetting>> GetSettings(string vin, string oneId)
    {
        var ret = await _db.TBoxNotificationSettings.Where(x => x.VIN == vin && x.OneId == oneId).ToArrayAsync();
        return ret;
    }

    public async Task AddOrUpdate(TBoxNotificationSettingRequestData data)
    {
        if (data is null) return;

        var temp = await _db.TBoxNotificationSettings.FirstOrDefaultAsync(x => x.VIN == data.VIN && x.OneId == data.OneId && x.Dxid == data.Dxid);
        if (temp is not null)
        {
            temp.StartupNotification = TryConvertToBool(data.StartupNotification);
            temp.LowBatteryNotification = TryConvertToBool(data.LowBatteryNotification);
            temp.StopWithoutLockNotification = TryConvertToBool(data.StopWithoutLockNotification);
            temp.AntiThieftFailureNotification = TryConvertToBool(data.AntiThieftFailureNotification);
            temp.MovingFailureNotification = TryConvertToBool(data.MovingFailureNotification);
            temp.MdifiedTime = DateTime.Now;
        }
        else
        {
            var now = DateTime.Now;
            _db.TBoxNotificationSettings.Add(new TBoxNotificationSetting
            {
                VIN = data.VIN,
                OneId = data.OneId,
                Dxid = data.Dxid,
                StartupNotification =  TryConvertToBool(data.StartupNotification),
                LowBatteryNotification = TryConvertToBool(data.LowBatteryNotification),
                StopWithoutLockNotification = TryConvertToBool(data.StopWithoutLockNotification),
                AntiThieftFailureNotification = TryConvertToBool(data.AntiThieftFailureNotification),
                MovingFailureNotification = TryConvertToBool(data.MovingFailureNotification),
                CreatedTime = now,
                MdifiedTime = now
            });
        }
        _db.SaveChanges();
    }

    static bool TryConvertToBool(dynamic? data)
    {
        if (bool.TryParse(Convert.ToString(data ?? ""), out bool b))
        {
            return b;
        }
        return false;
    }

    public async Task<TBoxNotificationSetting?> GetSettings(string vin, string oneId, string dxid)
    {
        if (string.IsNullOrEmpty(dxid))
            dxid = oneId;

        var ret = await _db.TBoxNotificationSettings.FirstOrDefaultAsync(x => x.VIN == vin && x.OneId == oneId && x.Dxid == dxid);
        return ret;
    }
}

public interface ITBoxNotificationSettingRepository
{
    Task<IEnumerable<TBoxNotificationSetting>> GetSettings(string vin, string oneId);
    Task AddOrUpdate(TBoxNotificationSettingRequestData data);
    Task<TBoxNotificationSetting?> GetSettings(string vin, string oneId, string dxid);
}