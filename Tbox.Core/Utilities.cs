﻿using Microsoft.AspNetCore.Http;

namespace Tbox.Core;

public static class Utilities
{
    /// <summary>
    /// Calculate distance between two coodinate
    /// </summary>
    /// <param name="d1"></param>
    /// <param name="d2"></param>
    /// <returns></returns>
    public static double CalculateDistance((double Lat, double Lon) c1, (double Lat, double Lon) c2)
    {
        const double EARTH_RADIUS = 6378.137; // km
        (double rad1lat, double rad1lon) = (ConvertToRadians(c1.Lat), ConvertToRadians(c1.Lon));
        (double rad2lat, double rad2lon) = (ConvertToRadians(c2.Lat), ConvertToRadians(c2.Lon));
        (double deltaLat, double deltaLot) = (rad2lat - rad1lat, rad2lon - rad1lon);
        return 2 * Math.Asin(
            Math.Sqrt(
                Math.Pow(deltaLat / 2, 2) + Math.Cos(rad1lat) * Math.Cos(rad2lat) * Math.Pow(Math.Sin(deltaLot / 2), 2)
            )
        ) * EARTH_RADIUS;
    }

    static double ConvertToRadians(double d)
        => d * Math.PI / 180;

    public static string ComputeTravelTimeAsHM(long epochStart, long epochEnd)
    {
        const int oneHourMinutes = 60;

        var startOffset = DateTimeOffset.FromUnixTimeSeconds(epochStart);
        var endOffset = DateTimeOffset.FromUnixTimeSeconds(epochEnd);

        var soDropSeconds = new DateTime(startOffset.Year, startOffset.Month, startOffset.Day, startOffset.Hour, startOffset.Minute, 0);
        var eoDropSeconds = new DateTime(endOffset.Year, endOffset.Month, endOffset.Day, endOffset.Hour, endOffset.Minute, 0);

        var travelTimeMinutes = eoDropSeconds.Subtract(soDropSeconds).TotalMinutes;

        var tHH = (int)travelTimeMinutes / oneHourMinutes;
        var tMM = (int)(travelTimeMinutes - (tHH * oneHourMinutes));

        return $"{tHH:00}:{tMM:00}";
    }

    public static (string validOneId, string validVIN) TryGetValidDataFromHeader(HttpRequest httpRequest)
    {
        var headers = httpRequest.Headers;
        var validOneId = headers[Constants.HeaderName.VALID_ONEID_KEY_NAME].FirstOrDefault();
        var validVIN = headers[Constants.HeaderName.VALID_VIN_KEY_NAME].FirstOrDefault();
        return (validOneId ?? string.Empty, validVIN ?? string.Empty);
    }
}