﻿namespace Tbox.Core.Extensions;

public static class DataConverter
{
    public static DateTime? ConvertTo(this string data)
    {
        if (string.IsNullOrEmpty(data))
            return null;

        if (data.Length == 8)
        {
            var by = int.TryParse(data.AsSpan(0, 4), out var y);
            var bm = int.TryParse(data.AsSpan(4, 2), out var m);
            var bd = int.TryParse(data.AsSpan(6, 2), out var d);
            if (by && bm && bd)
            {
                return new DateTime(y, m, d);
            }
        }
        return null;
    }
}