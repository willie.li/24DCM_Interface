﻿namespace Tbox.Core.Models;

public sealed record GetCarNameModel
{
    public string? VIN { get; init; }
    public string? CarNM { get; init; }
    public string? CurrentOneId { get; init; }
    public string? TOSSTS { get; init; }
    public string? TOSOneId { get; init; }
    public bool IsCarOwner { get; init; }
}