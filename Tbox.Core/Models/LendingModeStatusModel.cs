﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Tbox.Core.Models
{
    public class LendingModeStatusModel
    {
        [JsonPropertyName("scenariomode")]
        public int Scenaiomode { get; set; }
        [JsonPropertyName("lat")]
        public int Lat { get; set; }
        [JsonPropertyName("lon")]
        public int Lon { get; set; }
        [JsonPropertyName("radius")]
        public int Radius { get; set; }
    }
}
