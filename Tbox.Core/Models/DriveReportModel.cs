﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

public sealed record DriveReportModel
{
    [JsonPropertyName("total")]
    public int Total { get; init; }
    [JsonPropertyName("reports")]
    public IEnumerable<ReportDetailModel>? Reports { get; init; }
}

public sealed record ReportDetailModel
{
    [JsonPropertyName("trackid")]
    public long TrackId { get; init; }
    [JsonPropertyName("travelDistance")]
    public int TravelDistance { get; init; }
    [JsonPropertyName("epochStart")]
    public long EpochStart { get; init; }
    [JsonPropertyName("epochEnd")]
    public long EpochEnd { get; init; }
    [JsonPropertyName("avgFuelConsumption")]
    public int AvgFuelConsumption { get; init; }
    [JsonPropertyName("evPercentage")]
    public int EvPercentage { get; init; }
}