﻿namespace Tbox.Core.Models;

public class FCMPushModel
{
    public string Id { get; set; } = "";
    public string VIN { get; set; } = "";
    public string LICSNO { get; set; } = "";
    public long Epoch { get; set; }
    public int Status { get; set; }
    public string StatusName { get; set; } = "";
    public string Token { get; set; } = "";
    public string TypeName { get; set; } = "";
    public string Title { get; set; } = "";
    public string Body { get; set; } = "";
    public bool IsSendNotification { get; set; }
    public string OneId { get; set; } = "";
}