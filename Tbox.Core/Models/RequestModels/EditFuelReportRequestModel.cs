﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tbox.Core.Models.RequestModels;

public sealed class EditFuelReportRequestModel
{
    public string VIN { get; set; } = string.Empty;
    public string OneId { get; set; } = string.Empty;
    public string Id { get; set; } = string.Empty;
    public double Amount { get; set; }
    public double FuelDiff { get; set; }
    public int FuelType { get; set; }
}