﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models.RequestModels;

public class VehicleEventRequestModel
{
    public string VIN { get; set; } = "";
    /// <summary>
    /// a unit timestamp
    /// </summary>
    public long Epoch { get; set; }
    /// <summary>
    /// from EventStatusEnum
    /// status 8 = 借車模式
    /// </summary>
    public string Status { get; set; } = "";
    /// <summary>
    /// 借車模式，default 0 while status != 8
    /// </summary>
    [JsonPropertyName("scenariomode")]
    public int ScenarioMode { get; set; }
}