﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Tbox.Core.Models.RequestModels;

public sealed class SetVersionAgreementRequestModel
{
    public string VIN { get; set; } = string.Empty;
    public string OneId { get; set; } = string.Empty;
    public string CustId { get; set; } = string.Empty;
    public dynamic IsAgree { get; set; } = false;
    public string VersionAgreed { get; set; } = string.Empty;
    public string LICSNO { get; set; } = string.Empty;
    public string IP { get; set; } = string.Empty; 
}