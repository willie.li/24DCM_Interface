﻿namespace Tbox.Core.Models.RequestModels;

public sealed class DeletingRequestDataModel
{
    public string Vin { get; set; } = string.Empty;
    public long TrackId { get; set; }
}