﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tbox.Core.Models.RequestModels;

public sealed class TBoxNotificationSettingRequestData
{
    public string OneId { get; set; } = string.Empty;
    public string Dxid { get; set; } = string.Empty;
    public string VIN { get; set; } = string.Empty;
    public dynamic StartupNotification { get; set; } = false;
    public dynamic LowBatteryNotification { get; set; } = false;
    public dynamic StopWithoutLockNotification { get; set; } = false;
    public dynamic AntiThieftFailureNotification { get; set; } = false;
    public dynamic MovingFailureNotification { get; set; } = false;
}