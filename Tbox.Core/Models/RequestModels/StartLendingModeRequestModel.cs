﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models.RequestModels;

public class StartLendingModeRequestModel
{
    [JsonPropertyName("scenariomode")]
    public int ScenarioMode { get; set; }
    [JsonPropertyName("lat")]
    public int Lat { get; set; }
    [JsonPropertyName("lon")]
    public int Lon { get; set; }
    [JsonPropertyName("radius")]
    public int Radius { get; set; }
}