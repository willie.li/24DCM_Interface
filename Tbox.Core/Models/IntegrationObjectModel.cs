﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

public class IntegrationObjectModel
{
    [JsonPropertyName("trackid")]
    public long TrackId { get; set; }
    [JsonPropertyName("epochStart")]
    public long EpochStart { get; set; }
    [JsonPropertyName("epochEnd")]
    public long EpochEnd { get; set; }
    /// <summary>
    /// 行駛時間 EpochEnd(ms) - EpochStart(ms) => hh:mm
    /// </summary>
    [JsonPropertyName("travelTime")]
    public string? TravelTime { get; set; }

    /// <summary>
    /// 單位：百公尺
    /// </summary>
    [JsonPropertyName("travelDistance")]
    public int TravelDistance { get; set; }
    [JsonPropertyName("AvgFuelConsumption")]
    public int AvgFuelConsumption { get; set; }
    /// <summary>
    /// 平均車速 TravelDistance(m) / TravelTime(s) => km/h
    /// </summary>
    [JsonPropertyName("avgSpeed")]
    public int AvgSpeed { get; set; }

    /// <summary>
    /// 擴充屬性：是否有正常位移資料
    /// </summary>
    [JsonPropertyName("hasRegularResults")]
    public bool HasRegularResults { get; set; }
    /// <summary>
    /// 擴充屬性：正常位移資料
    /// </summary>
    [JsonPropertyName("regularResults")]
    public IEnumerable<GpsInfoModel>? RegularResults { get; set; }

    /// <summary>
    /// 擴充屬性：是否有異常位移資料
    /// </summary>
    [JsonPropertyName("hasAbnormalResults")]
    public bool HasAbnormalResults { get; set; }
    /// <summary>
    /// 擴充屬性：異常位移資料
    /// </summary>
    [JsonPropertyName("abnormalResults")]
    public IEnumerable<TrackingInfoModel>? AbnormalResults { get; set; }
    /// <summary>
    /// 擴充屬性：異常位移距離（異常位移點 >= 2 個以上，最後一個座標、首個座標間的距離）
    /// </summary>
    [JsonPropertyName("abnormalMovingDistance")]
    public double AbnormalMovingDistance { get; set; }

    [JsonPropertyName("evPercentage")]
    public int EvPercentage { get; set; }
}