﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

/// <summary>
/// CMX01:查詢車輛GPS位置
/// </summary>
public sealed record GpsInfoModel
{
    [JsonPropertyName("id")]
    public string? Id { get; set; }
    [JsonPropertyName("vin")]
    public string? Vin { get; set; }
    [JsonPropertyName("trackid")]
    public long TrackId { get; set; }
    [JsonPropertyName("epoch")]
    public long Epoch { get; set; }
    [JsonPropertyName("lat")]
    public decimal Lat { get; set; }
    [JsonPropertyName("lon")]
    public decimal Lon { get; set; }
    [JsonPropertyName("speed")]
    public int Speed { get; set; }
    [JsonPropertyName("angle")]
    public int Aangle { get; set; }

    [JsonPropertyName("vehicleStatus")]
    public int VehicleStatus { get; set; }
    [JsonPropertyName("vehicleStatusName")]
    public string? VehicleStatusName { get; set; }
    [JsonPropertyName("gpsStatus")]
    public int GpsStatus { get; set; }
    [JsonPropertyName("gpsStatusName")]
    public string? GpsStatusName { get; set; }
    [JsonPropertyName("speedy")]
    public int Speedy { get; set; }
}

public enum VehicleStatusEnum
{
    正常移動 = 1,
    異常移位 = 2
}

public enum GpsStatusEnum
{
    已定位 = 1,
    未定位 = 2
}