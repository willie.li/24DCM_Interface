﻿namespace Tbox.Core.Models;

public sealed record FuelReportResponseModel
{
    public int TotalCounts { get; init; }
    public int FuelType { get; init; }
    public double FuelPrice { get; init; }
    public double TotalFuelPrice { get; init; }
    public double TotalTravelDistance { get; init; }
    public IEnumerable<FuelDetailModel>? Detail { get; init; }
    public int CurrentPage { get; init; }
    public int MaxItemsPerPage { get; init; }
}

public sealed record FuelDetailModel
{
    public string? Id { get; init; }
    public string? VIN { get; init; }
    public long Epoch { get; init; }
    public double AvgFuelConsum { get; init; }
    public double FuelConsumption { get; init; }
    public double TravelDistance { get; init; }
    public double FuelDiff { get; init; }
    public double Lat { get; init; }
    public double Lon { get; init; }
    public double Amount { get; init; }
}