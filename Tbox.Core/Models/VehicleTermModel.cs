﻿namespace Tbox.Core.Models;

public sealed record VehicleTermModel
{
    public string? VIN { get; init; }
    public string? ONEID { get; init; }
    public string? ONEIDSTS { get; init; }
    public string? TOSSTS { get; init; }
    public string? TOSVER { get; init; }
    public string? CUSTID { get; init; }
}