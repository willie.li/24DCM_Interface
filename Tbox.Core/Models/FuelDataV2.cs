﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

public sealed record FuelDataV2
{
    [JsonPropertyName("data")]
    public Data? Data { get; init; }
}

public sealed record Data
{
    [JsonPropertyName("updated_at")]
    public DateTime? UpdatedAt { get; init; }
    [JsonPropertyName("current")]
    public Current? Current { get; init; }
    [JsonPropertyName("in_comming")]
    public dynamic? Incomming { get; init; }
    [JsonPropertyName("predict")]
    public Predict? Predict { get; init; }
}

public sealed record Current
{
    [JsonPropertyName("gas_92")]
    public string? Gas92 { get; init; }
    [JsonPropertyName("gas_95")]
    public string? Gas95 { get; init; }
    [JsonPropertyName("gas_98")]
    public string? Gas98 { get; init; }
    [JsonPropertyName("diesel")]
    public string? Diesel { get; init; }
    [JsonPropertyName("start_at")]
    public DateTime? StartAt { get; init; }
    [JsonPropertyName("end_at")]
    public DateTime? EndAt { get; init; }
}

public class Predict
{
    [JsonPropertyName("gas_92")]
    public string? Gas92 { get; init; }
    [JsonPropertyName("gas_95")]
    public string? Gas95 { get; init; }
    [JsonPropertyName("gas_98")]
    public string? Gas98 { get; init; }
    [JsonPropertyName("diesel")]
    public string? Diesel { get; init; }
    [JsonPropertyName("start_at")]
    public DateTime? StartAt { get; init; }
    [JsonPropertyName("end_at")]
    public DateTime? EndAt { get; init; }
}