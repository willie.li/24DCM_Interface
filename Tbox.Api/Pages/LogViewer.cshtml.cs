using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Tbox.Api.Pages;

public class LogViewerModel : PageModel
{
    private readonly IHttpContextAccessor _contextAccessor;
    private readonly ILogger<LogViewerModel> _logger;
    private readonly string _contentRootPath;

    public LogViewerModel(IWebHostEnvironment env, IHttpContextAccessor contextAccessor, ILogger<LogViewerModel> logger)
    {
        _contextAccessor = contextAccessor;
        _logger = logger;
        _contentRootPath = env.ContentRootPath;
    }

    public IAsyncEnumerable<string>? LogData { get; private set; }
    public string? Error { get; private set; }

    public IActionResult OnGet(string dt, bool dl = false)
    {
        if (!Strings.CheckAccessable(_contextAccessor, _logger))
            return NotFound();

        var oneDay = DateTime.Today;
        if (DateTime.TryParse(dt, out var d))
        {
            oneDay = d;
        }

        try
        {
            if (dl)
            {
                var dlFname = $"applogs{oneDay:yyyyMMdd}.log";
                var dllp = Path.Combine($"{_contentRootPath}", "logs", dlFname);
                var dlfs = new FileStream(dllp, new FileStreamOptions
                {
                    Access = FileAccess.Read,
                    Mode = FileMode.Open,
                    Share = FileShare.ReadWrite
                });
                return File(dlfs, "text/plain", dlFname);
            }
            var lp = Path.Combine($"{_contentRootPath}", "logs", $"applogs{oneDay:yyyyMMdd}.log");
            LogData = Strings.ReadLogPerLing(lp);
            return Page();
        }
        catch (Exception ex)
        {
            Error = $"{ex.Message}\n{ex.InnerException?.Message}";
            return Page();
        }
    }
}