﻿namespace Tbox.Api.Endpoints.VehicleInfo;

public sealed class CreateFuelReportEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPost($"{Constants.VehicleInfoEndpointsPrefix}/add-fuel", CreateFuelReport)
            .WithTags(Constants.VehicleInfoTagName)
            .WithName(nameof(CreateFuelReport))
            .WithDisplayName("接收加油記錄")
            .WithSummary("實作 HT04:接收加油記錄")
            .WithOpenApi()
            .AddEndpointFilter<CmxEntryFilter>();
    }

    static IResult CreateFuelReport([FromServices] IDataAccessService dataAccessService,
        [FromBody] CreateFuelReportRequestModel req)
    {
        if (req is null || string.IsNullOrEmpty(req.VIN))
        {
            return Results.BadRequest("missing parameter");
        }

        var check = dataAccessService.FuelReports.GetCheckForFuelReport(req.VIN,req.Epoch);

        // chekc existed data
        if (check?.VIN == req.VIN || check?.Epoch == req.Epoch) { return Results.Problem(detail: "重複資料", statusCode: 200);}

        var result =dataAccessService.FuelReports.Create(new FuelReport {
            VIN = req.VIN,
            Epoch = req.Epoch,
            AvgFuelConsum = req.AvgFuelConsum,
            FuelConsumption = req.FuelConsumption,
            TravelDistance = req.TravelDistance,
            FuelDiff = (int)req.FuelDiff,
            Lat = (req.Lat ?? 0) / 1_000_000d,
            Lon = (req.Lon ?? 0) / 1_000_000d,
            PermanentId = Guid.NewGuid().ToString(),
            CreatedTime= DateTime.Now
        }).Result;
        
        return result? Results.Ok():Results.Problem(detail: "寫入失敗", statusCode:400);
    }
}