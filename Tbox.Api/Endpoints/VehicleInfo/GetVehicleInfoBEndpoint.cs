﻿using Microsoft.Data.SqlClient;
using Tbox.Core.Extensions;

namespace Tbox.Api.Endpoints.VehicleInfo;

public sealed class GetVehicleInfoBEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.VehicleInfoEndpointsPrefix}/get_vehicle_info_b/{{obd2vin}}", GetVehicleInfoB)
            .WithTags(Constants.VehicleInfoTagName)
            .WithName(nameof(GetVehicleInfoB))
            .WithDisplayName("查詢交車與綁定資料")
            .WithSummary("實作 HT01-B：查詢交車與綁定資料")
            .WithDescription("查詢Lexus Server，從 ECHO 每日轉檔到 Lexus Server，<br/>當 Lexus Server 查無交車資料，再往 ECHO 查詢")
            .WithOpenApi()
            .AddEndpointFilter<CmxEntryFilter>();
    }

    static async Task<IResult> GetVehicleInfoB([FromServices] IEncryptedAppSettings<AppSettings> settings, [FromServices] IDataAccessService dataAccessService,
        [FromRoute] string obd2vin)
    {
        var t1 = DateTime.Now;

        //var vehicleVinRef = await dataAccessService.VehicleVinRefs.Get(obd2vin);
        //var vin = vehicleVinRef?.VIN;
        //if (string.IsNullOrEmpty(vin))
        //{
        //    return Results.BadRequest("查無交車資料");
        //}

        var b2 = await dataAccessService.VehicleInfoB2s.Get(obd2vin);
        using var mclubConn = new SqlConnection(settings.AppSettings.MclubDbConnection);
        var mclubService = new MclubService(mclubConn);
        var retail = await mclubService.GetCarRetailData(obd2vin);

        if (!string.IsNullOrEmpty(retail?.LICSNO?.Trim()))
        {
            await dataAccessService.VehicleInfoB2s.WriteBackLicenseNo(obd2vin, retail.LICSNO.Replace(" ", ""));
        }
        // close the connection

        var result = new BaseOutputModel<dynamic>
        {
            Data = new
            {
                LICSNO = retail?.LICSNO,
                STSCD = retail?.STSCD,
                WHBRNHNM = retail?.WHBRNHNM,
                REDLDT = retail?.REDLDT,
                REDLDT_r = retail?.REDLDT?.ConvertTo(),

                ONEID = b2?.ONEID.ToString(),
                ONEIDSTS = b2?.ONEIDSTS,
                TOSSTS = b2?.TOSSTS,
                TOSVER = b2?.TOSVER
            },
            Durations = (DateTime.Now - t1).TotalMilliseconds
        };
        mclubConn.Close();

        return Results.Ok(result);
    }
}