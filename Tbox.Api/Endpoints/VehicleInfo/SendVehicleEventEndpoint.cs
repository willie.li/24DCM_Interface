﻿using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Security.Cryptography;
using static Dapper.SqlMapper;

namespace Tbox.Api.Endpoints.VehicleInfo;

public sealed class SendVehicleEventEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPost($"{Constants.VehicleInfoEndpointsPrefix}/send_vehicle_event", SendVehicleEvent)
            .WithTags(Constants.VehicleInfoTagName)
            .WithName(nameof(SendVehicleEvent))
            .WithDisplayName("發送車況推播通知")
            .WithSummary("實作 HT02：發送車況推播通知")
            .WithOpenApi()
            .AddEndpointFilter<CmxEntryFilter>();
    }

    static async Task<IResult> SendVehicleEvent([FromServices] IHttpClientWrapper httpClient,
        [FromServices] IDataAccessService dataAccessService,
        [FromServices] IEncryptedAppSettings<AppSettings> appSettings,
        [FromBody] VehicleEventRequestModel req)
    {
        if (string.IsNullOrEmpty(req.VIN))
        {
            return Results.BadRequest("missing data or VIN");
        }

        // step 1. find oneid-dxid-token by vin
        var b2 = await dataAccessService.VehicleInfoB2s.Get(req.VIN);
        if (b2 is null)
        {
            return Results.NotFound($"{req.VIN} data NOT found.");
        }
        bool isBbeforeAfree = await dataAccessService.VehicleInfoB2s.CheckAgreetimeIsBeforeCurrentTime(req.VIN);
        if (isBbeforeAfree)
        {
            return Results.Ok("agreed before today");
        }

        // Tbox App 2.0, for notificationSetting, dxid set to oneId
        //var dxid = b2.ONEID;

        // step 2. send 1by1 notification via FCM
        // 2-1. 若是推播設定中無資料，則預設接收全部推播
        var oneid = b2.ONEID.ToString();
        var notificationSettings = await dataAccessService.TBoxNotificationSettings.GetSettings(req.VIN, oneid, oneid);
        notificationSettings ??= new TBoxNotificationSetting
        {
            VIN = req.VIN,
            OneId = oneid,
            StartupNotification = true,
            LowBatteryNotification = true,
            StopWithoutLockNotification = true,
            AntiThieftFailureNotification = true,
            MovingFailureNotification = true
        };

        var row = new FCMPushModel
        {
            LICSNO = b2.LICSNO ?? string.Empty,
            OneId = oneid
        };

        _ = int.TryParse(req.Status, out var status);
        var selectedRow = SelectNotificationType(status, row, notificationSettings, req.ScenarioMode);
        if (selectedRow is null)
        {
            return Results.BadRequest("status out of range");
        }
        if (!selectedRow.IsSendNotification)
        {
            return Results.NoContent();
        }

        var htapp20Url = appSettings.AppSettings.Htapp20ServiceUrl;
        var isTestMode = appSettings.AppSettings.IsTestAppMode;

        var inserted = await TboxMessageInsertToInbox20(httpClient, htapp20Url, oneid, selectedRow.Title, selectedRow.Body, req.Epoch);
        if (string.IsNullOrEmpty(inserted?.Id))
            return Results.Ok("duplicated api call.");

        row.Id = inserted.Id;
        var ret = await SendNotificationViaPlatform(dataAccessService, httpClient, htapp20Url, row, isTestMode);

        var effect = await SetLexusSendStatus(httpClient, htapp20Url,ret, row.Id);

        return Results.Ok(ret);
    }

    static FCMPushModel? SelectNotificationType(int status, FCMPushModel row, TBoxNotificationSetting notificationSetting, int scenarioMode = 0)
    {
        var licsno = !string.IsNullOrEmpty(row.LICSNO) ? $"（{row.LICSNO}）" : string.Empty;
        switch (status)
        {
            case 1:
                row.Title = "車輛發動通知";
                row.Body = $"您的愛車{licsno}已被發動，如非您本人所操作，建議您進行確認。\n\n*通知時間可能受車輛所在位置網路環境影響";
                row.IsSendNotification = notificationSetting.StartupNotification;
                break;

            case 2:
                row.Title = "電瓶低電壓通知";
                row.Body = $"您的愛車{licsno}偵測到電瓶電壓不足，建議您向LEXUS維修服務據點進行確認。\n\n*通知時間可能受車輛所在位置網路環境影響";
                row.StatusName = "車輛狀態頁";
                row.IsSendNotification = notificationSetting.LowBatteryNotification;
                break;

            case 3:
                row.Title = "電瓶低電壓通知";
                row.Body = $"您的愛車{licsno}偵測到電瓶電壓嚴重不足，建議您向LEXUS維修服務據點進行確認。\n\n* 因車輛電壓嚴重不足，為避免耗盡電力，將停止異常偵測服務\n* 通知時間可能受車輛所在位置網路環境影響";
                row.StatusName = "車輛狀態頁";
                row.IsSendNotification = notificationSetting.LowBatteryNotification;
                break;

            case 4:
                row.Title = "熄火未上鎖通知";
                row.Body = $"您的愛車{licsno}偵測到熄火後未完成上鎖，為確保個人財產安全，建議您前往確認並上鎖。\n\n*此為熄火後兩分鐘內狀態，若已上鎖請忽略此訊息\n*通知時間可能受車輛所在位置網路環境影響";
                row.StatusName = "車輛狀態頁";
                row.IsSendNotification = notificationSetting.StopWithoutLockNotification;
                break;

            case 5:
                row.Title = "異常移位通知";
                row.Body = $"您的愛車{licsno}偵測到異常移位，建議您前往車輛定位頁面確認。\n\n*通知時間可能受車輛所在位置網路環境影響";
                row.StatusName = "車輛定位頁";
                row.IsSendNotification = notificationSetting.MovingFailureNotification;
                break;

            case 7:
                row.Title = "防盜異常通知";
                row.Body = $"您的愛車{licsno}偵測到車輛防盜系統作動，為確保個人財產安全，建議您前往確認。\n\n*通知時間可能受車輛所在位置網路環境影響";
                row.IsSendNotification = notificationSetting.AntiThieftFailureNotification;
                break;

            case 8:
                switch (scenarioMode)
                {
                    case 1:
                        row.Title = "【借車模式-家人守護】進入範圍通知";
                        row.Body = "親愛的車主您好\n\n您的愛車已進入您透過「家人守護」模式所設定的範圍(目的地為中心，範圍500公尺內)，通知後不會自動關閉借車模式，如未經手動關閉，則愛車下次發動仍可能會再次收到此通知。";
                        row.IsSendNotification = true;
                        break;

                    case 2:
                        row.Title = "【借車模式- 出借管理】超出範圍通知";
                        row.Body = "親愛的車主您好\n\n您的愛車已超出您透過「出借管理」模式所設定的追蹤範圍(自訂20km-100km)，通知後不會自動關閉借車模式，如未經手動關閉，則愛車下次發動仍可能會再次收到此通知。";
                        row.IsSendNotification = true;
                        break;

                    case 3:
                        row.Title = "【借車模式-代客泊車】超出範圍通知";
                        row.Body = "親愛的車主您好\n\n您的愛車已超出您透過「代客泊車」模式所設定的範圍(愛車為中心點，範圍300公尺外)，通知後不會自動關閉借車模式，如未經手動關閉，則愛車下次發動仍可能會再次收到此通知。";
                        row.IsSendNotification = true;
                        break;

                    default:
                        //row = null;
                        //break;
                        return null;
                }
                break;

            case 99:
                row.Title = "系統測試推播訊息";
                row.Body = "此為系統不定期測試推播所用之訊息，目的係確保您的LEXUS LINK服務可隨時保持最佳狀態，並作為未來可持續提供更好的服務之用途";
                row.IsSendNotification = true;
                break;

            default:
                //row = null;
                //break;
                return null;
        }
        return row;
    }

    static async Task<TboxMessageData?> TboxMessageInsertToInbox20(IHttpClientWrapper httpClient, string htapp20ServiceUrl, string oneId, string title, string body, long epoch)
    {
        var req = new HttpRequestMessage
        {
            RequestUri = new Uri(htapp20ServiceUrl + "/api/1.0/tbox-add"),
            Method = HttpMethod.Post,
            Content = new StringContent(JsonSerializer.Serialize(new
            {
                oneId,
                title,
                body,
                epoch,
            }), System.Text.Encoding.UTF8, "application/json")
        };
        try
        {
            httpClient.IsWriteLog = true;

            var Request = req;
            httpClient.Send(Request);

            using var resp = httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<TboxMessageData>(content);
                return data;
            }
            return null;
        }
        catch
        {
#if DEBUG
            throw;
#else
            return null;
#endif
        }
    }

    static async Task<int> SetLexusSendStatus(IHttpClientWrapper httpClient, string htapp20ServiceUrl,string  sentResult, string id)
    {
        var req = new HttpRequestMessage
        {
            RequestUri = new Uri(htapp20ServiceUrl + "/api/1.0/tbox-sentresult"),
            Method = HttpMethod.Put,
            Content = new StringContent(JsonSerializer.Serialize(new
            {
                sentResult,
                id,
            }), System.Text.Encoding.UTF8, "application/json")
        };
        try
        {
            httpClient.IsWriteLog = true;

            var Request = req;
            httpClient.Send(Request);

            using var resp = httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                //var data = JsonSerializer.Deserialize<int>(content);
                return 1;
            }
            return -1;
        }
        catch
        {
#if DEBUG
            throw;
#else
            return -1;
#endif
        }
    }


    static async Task<string> SendNotificationViaPlatform(IDataAccessService dataAccessService, IHttpClientWrapper httpClient,
        string htapp20ServiceUrl, FCMPushModel row, bool isTestAppMode)
    {
        var appId = isTestAppMode ? (int)FcmAppId.LexusPlusTest : (int)FcmAppId.LexusPlus;
        // NOTE: registerId MUST NOT set to 「 0 」, if so, notification will send to all registered users
        long registerId = await GetRegisterId(dataAccessService, row.OneId, appId);
        if (registerId <= 0)
            return $"registerId: {registerId} is NOT exist, skip sending notification.";

        var pfcmReq = new BatchSendRequestModel
        {
            AppId = appId,
            RegisterIds = new[] { registerId },
            CategoryCode = "19", // Tbox type
            MessageTypeCode = "03",
            Title = $"{row.Title}",
            Content = $"{row.Body}",
            //WebUrl = $"https://lexusapp-fd-prod-01.azurefd.net/fe-api/TboxMsg/{row.Id}",
            //WebUrl = "https://htapp-api-dev.hotaicloud.com/tboxMsg/" + row.Id,
            WebUrl = htapp20ServiceUrl + "/TboxMsg/" + row.Id,
            UserId = "HT02"
        };
        var result_platform = await BatchSend(httpClient, pfcmReq);
        var pq = JsonSerializer.Serialize(pfcmReq);
        var pr = string.Empty;
        if (result_platform?.IsSuccessfuleGrabData == true
            && result_platform?.Data?.Data is not null)
        {
            pr = JsonSerializer.Serialize(result_platform.Data.Data);
        }
        var sc = string.Empty;
        sc = JsonSerializer.Serialize(result_platform.StatusCode);
        return $"StatusCode : {sc}\n{pr}";
    }

    static async Task<long> GetRegisterId(IDataAccessService dataAccessService, string oneId, int appId)
    {
        long registerId = await dataAccessService.FcmInfos.GetRegisterId(appId, oneId);
        return registerId;
    }

    static string GenTokenKey(string scope)
        => $"{Constants.GlobalHtFCMTokenKeyName}::{scope}";

    static async Task<HtFcmTokenModel?> GetToken(IHttpClientWrapper httpClient, string scope)
    {

        var tk = AppDomain.CurrentDomain.GetData(GenTokenKey(scope));
        if (tk is not HtFcmTokenModel validtk)
        {
            var ret = await TryGetToken(httpClient, scope);
            if (ret?.IsSuccessfuleGrabData == true)
            {
                return ret.Data;
            }
            return null;
        }
        else
        {
            var preTk = tk.GetType().GetProperty("AccessToken").GetValue(tk, null).ToString();
            var testtk = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjJDU1pFRWhhak5kZ2dDNVdCTEhtZ1EiLCJ0eXAiOiJhdCtqd3QifQ.eyJuYmYiOjE2OTMzODcxMDksImV4cCI6MTY5MzQ3MzUwOSwiaXNzIjoiaHR0cHM6Ly9zc2lkZW50aXR5c2VydmVycHJvZC5henVyZXdlYnNpdGVzLm5ldCIsImF1ZCI6InNzcHVzaCIsImNsaWVudF9pZCI6IndlYmNsaWVudCIsInNjb3BlIjpbInNzcHVzaCJdfQ.ZzVF6F0R4ixNO7iHYtd8aHO49CF77UUMVF6IKg0pddax2D58hlS-v7DexwiXamp0qklprfBKIV1fopNWolJniMdZoWNyxHtenxW-dmBhfA5Z6rVhzW0C7LxitSoRaVx0-bsRHkWcwPkhGPsa8mgLY6-8x5fLrIGSrxhOGz22u08rfT8q1hoN04P-GNZ_1KtuQvF11VTC-4SaKdpkEEhOM8lnrOcGDGFSf-pNkgSetQFENZW3rsZ7p73mhyPprHg0QOEO3hEFXs_fgnNNYXs0ze0k4co9Q3yC8y4QSMYXHtOsOaqgr8ytxP-nnQmpvGGMAPy_DfBWHICeM1mIRZLqpw";
            //compare the token expire Time
            var handler = new JwtSecurityToken(jwtEncodedString: preTk);
            var jti = handler.Claims.First(x => x.Type == "exp").Value;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(jti));
            DateTime expT = dateTimeOffset.UtcDateTime;
            if (expT < DateTime.UtcNow)
            {
                var ret = await TryGetToken(httpClient, scope);
                if (ret?.IsSuccessfuleGrabData == true)
                {
                    return ret.Data;
                }
                return null;
            }  
        }
        return validtk;
    }

    static async Task<BaseResponseModel<HtFcmTokenModel>> TryGetToken(IHttpClientWrapper httpClient, string scope)
    {
        // 根據要使用的API類型，API 1-2 傳入:  ssregister,  API 1-3、API 1-5 傳入:   ssmaintain, API 1-4、API 1-6 傳入:  sspush
        // doc: 2022-3-28：根據要使用的API類型，API 1-2、API 1-6 傳入:  ssregister,  API 1-3、API 1-5 傳入:   ssmaintain, API 1-4 傳入:  sspush
        var req = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri("https://ssidentityserverprod.azurewebsites.net/connect/token"),
            Content = new FormUrlEncodedContent(new Dictionary<string, string> {
                    { "client_id", "webclient" },
                    { "client_secret", "53206d4e-687b-48ce-94ab-53099e897007" },
                    { "scope", scope },
                    { "grant_type", "client_credentials" }
                })
        };
        try
        {
            var Request = req;
            httpClient.Send(Request);
            using var resp = httpClient.Response;
            var content = await resp?.Content.ReadAsStringAsync();
            if (resp?.IsSuccessStatusCode == true)
            {
                var token = JsonSerializer.Deserialize<HtFcmTokenModel>(content);
                //RC.Default.Set(GenTokenKey(scope), token, DateTimeOffset.Now.AddHours(23.5));




                AppDomain.CurrentDomain.SetData(GenTokenKey(scope), token);

                return new BaseResponseModel<HtFcmTokenModel>
                {
                    IsSuccessfuleGrabData = true,
                    StatusCode = (int)OK,
                    Data = token,
                };
            }
            else
            {
                Log.Warning($"nameof{TryGetToken}|Error: {resp?.StatusCode} | Message: GetSendVehicle push Token failed || Token:{AppDomain.CurrentDomain.GetData(GenTokenKey(scope))} ");
                return new BaseResponseModel<HtFcmTokenModel>
                {
                    StatusCode = (int)(resp?.StatusCode ?? BadRequest),
                    ErrorMessage = content,
                };
            }
        }
        catch (Exception ex)
        {
            return new BaseResponseModel<HtFcmTokenModel>
            {
                StatusCode = (int)InternalServerError,
                ErrorMessage = $"{ex.Message}\n{ex.InnerException?.Message}",
            };
        }
    }

    static async Task SetAuthorizationHeader(IHttpClientWrapper httpClient, string scope)
    {
        httpClient.HttpClientInstance.DefaultRequestHeaders.Clear();

        var tk = await GetToken(httpClient, scope);
        httpClient.HttpClientInstance.DefaultRequestHeaders.Add("Authorization", $"Bearer {tk?.AccessToken}");
    }

    static async Task<BaseResponseModel<HtFcmBaseResponseModel<BatchSendResponseModel>>> BatchSend(IHttpClientWrapper httpClient, BatchSendRequestModel data)
    {
        var req = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri($"https://sspushserverprod.azurewebsites.net/api/notification/BatchPushMessage"),
            Content = new StringContent(JsonSerializer.Serialize(data), System.Text.Encoding.UTF8, "application/json")
        };
        try
        {
            await SetAuthorizationHeader(httpClient, "sspush");

            var Request = req;
            httpClient.Send(Request);
            using var resp = httpClient.Response;
            var content = await resp?.Content.ReadAsStringAsync();
            Log.Information($"BatchSend StatusCode:{resp.StatusCode} ||Req: {JsonSerializer.Serialize(data)} || Resp:{content}");
            if (resp?.IsSuccessStatusCode == true)
            {
                if(resp.StatusCode == Unauthorized)
                {
                    return new BaseResponseModel<HtFcmBaseResponseModel<BatchSendResponseModel>>
                    {
                        IsSuccessfuleGrabData = false,
                        StatusCode = (int)Unauthorized,
                        Data = null
                    };
                }
                var ret = JsonSerializer.Deserialize<HtFcmBaseResponseModel<BatchSendResponseModel>>(content);
                return new BaseResponseModel<HtFcmBaseResponseModel<BatchSendResponseModel>>
                {
                    IsSuccessfuleGrabData = true,
                    StatusCode = (int)OK,
                    Data = ret,
                };
            }
            Log.Information($"BatchSend StatusCode:{resp.StatusCode} ||Req: {JsonSerializer.Serialize(data)} || Resp:{content}");
            return new BaseResponseModel<HtFcmBaseResponseModel<BatchSendResponseModel>>
            {
                StatusCode = (int)resp.StatusCode,
                ErrorMessage = $"Batch Sent Failed|{content}",
            };
        }
        catch (Exception ex)
        {
            Log.Information($"BatchSend StatusCode:{(int)InternalServerError} ||Req: {JsonSerializer.Serialize(data)} || Resp:{ex.Message}");
            return new BaseResponseModel<HtFcmBaseResponseModel<BatchSendResponseModel>>
            {
                StatusCode = (int)InternalServerError,
                ErrorMessage = $"{ex.Message}\n{ex.InnerException?.Message}",
            };
        }
    }
}

public sealed class BaseResponseModel<T>
{
    public string RequestId { get; } = Guid.NewGuid().ToString();
    public bool IsSuccessfuleGrabData { get; set; }
    public int StatusCode { get; set; }
    public T? Data { get; set; }
    public string? ErrorMessage { get; set; }
    public double Durations { get; set; }
}

public record HtFcmTokenModel
{
    [JsonPropertyName("access_token")]
    public string? AccessToken { get; init; }
    [JsonPropertyName("expires_in")]
    public int ExpiresIn { get; init; }
    [JsonPropertyName("token_type")]
    public string? TokenType { get; init; }
    [JsonPropertyName("scope")]
    public string? Scope { get; init; }
}

public record HtFcmBaseResponseModel<T>
{
    public bool Result { get; init; }
    public string? Message { get; init; }
    public T? Data { get; init; }
}

public sealed record BatchSendResponseModel
{
    public int BatchNo { get; init; }
    public IEnumerable<int>? MessageIds { get; init; }
}

public enum FcmAppId
{
    // 正式版
    LexusPlus = 18,

    // 測試版
    LexusPlusTest = 27,
}

public sealed class BatchSendRequestModel
{
    /// <summary>
    /// AppId from API 1-2
    /// </summary>
    public int AppId { get; set; }
    /// <summary>
    /// registerId from API 1-2，接收對象推播註冊ID陣列 e.g. [10,12,33]
    /// </summary>
    public IEnumerable<long>? RegisterIds { get; set; }
    /// <summary>
    /// 固定 0
    /// </summary>
    [JsonPropertyName("MessageId")]
    public int MessageId { get; set; } = 0;
    /// <summary>
    /// 訊息標題(最長50個字)
    /// </summary>
    public string? Title { get; set; }
    /// <summary>
    /// 訊息內容(最長500個字)
    /// </summary>
    public string Content { get; set; } = "";
    /// <summary>
    /// 訊息類別：01 純⽂文字，02 圖片；03 網頁
    /// </summary>
    public string MessageTypeCode { get; set; } = "01";
    /// <summary>
    /// 訊息分類，僅做為前端需要時使用(自訂義欄位)
    /// </summary>
    public string CategoryCode { get; set; } = "";
    /// <summary>
    /// 圖片網址,前端可透過此欄位取得要顯示的圖片(MessageTypeCode=02時可來取用此欄位)
    /// </summary>
    public string ImageUrl { get; set; } = "";
    /// <summary>
    /// 網頁網址,前端可透過此欄位取得要顯示的圖片(MessageTypeCode=03時可來取用此欄位)
    /// </summary>
    public string WebUrl { get; set; } = "";
    /// <summary>
    /// 分享到"的功能,0=無,1=facebook, 2=twitter, 4=google+, 8=pinterest, 如果要多項可將其值相加,例如:7=facebook+twitter+google (要配合前端app支援此功能)
    /// </summary>
    public int ShareTo { get; set; } = 0;
    /// <summary>
    /// 開啟外部連結(網頁)的tilte內容,可設定在按鍵上的文字內容(要配合前端app支援此功能)
    /// </summary>
    public string ExternalTitle { get; set; } = "";
    /// <summary>
    /// 啟外部連結的網址(要配合前端app支援此功能)
    /// </summary>
    public string ExternalUrl { get; set; } = "";
    /// <summary>
    /// 按下外部連結網頁後的畫面抬頭文字內容(要配合前端app支援此功能)
    /// </summary>
    public string Page2Title { get; set; } = "";
    /// <summary>
    /// 透過第Page2開啟的第3頁抬頭(要配合前端app支援此功能)
    /// </summary>
    public string Page3Title { get; set; } = "";
    /// <summary>
    /// 透過第Page2開啟的第3頁文字內容(要配合前端app支援此功能)
    /// </summary>
    public string Page3Content { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info01 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info02 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info03 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info04 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info05 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info06 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info07 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info08 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info09 { get; set; } = "";
    /// <summary>
    /// 使用者自定義欄位
    /// </summary>
    public string Info10 { get; set; } = "";
    /// <summary>
    /// 資料新增者，oneId？
    /// </summary>
    public string UserId { get; set; } = "";
    /// <summary>
    /// 息訊保留截止日(unixtime),如果APP移除後重新安裝,在此日期內的所有訊息會重新下載到APP端
    /// </summary>
    public string EndTime { get; set; } = $"{DateTime.Today.AddDays(-1):yyyy-MM-ddTHH:mm:ss.fffZ}"; // 2021-06-24T08:12:16.541Z
    /// <summary>
    /// 放 0
    /// </summary>
    public int BatchNo { get; set; } = 0;
    /// <summary>
    /// 指定推播時間,如未指定或指定時間小於當前時間,代表立即推送(以後台接收到的時間點為準,如果有設分批,會以這個時間點為起始時間),格式範例: 2021-06-23T03:02:27.577Z
    /// </summary>
    public string ScheduledTime { get; set; } = $"{DateTime.Today.AddDays(-1):yyyy-MM-ddTHH:mm:ss.fffZ}";
    /// <summary>
    /// 將指定RegisterIds 的內容分批次推送(此值為批次數量),每批數量為( RegisterIds的數量 / Batchs ),如果為1代表不分批(此值必需>0)
    /// </summary>
    public int Batches { get; set; } = 1;
    /// <summary>
    /// 分批之間的間隔時間,單位為秒(此值必需大於60)
    /// </summary>
    public int BatchInterval { get; set; } = 60;
    /// <summary>
    /// 後台使用,請固定值為null 值
    /// </summary>
    // public string FromRegisterDate { get; set; }
    /// <summary>
    /// 後台使用,請固定值為null 值
    /// </summary>
    // public string ToRegisterDate { get; set; }
    /// <summary>
    /// 後台使用,請固定值為null 值
    /// </summary>
    // public int PlatformCode { get; set; }
    /// <summary>
    /// 推播對象沒有指定(也就是沒給RegisterIds )時,此欄位才有作用,
    /// "01"表示沒有再度過濾條件,
    /// "02"則會以下列欄位做為過濾條件(如果這些欄位為null,則忽略該條
    /// </summary>
    public string ListType { get; set; } = "01";
    /// <summary>
    /// 後台使用,請固定值為 "" 值
    /// </summary>
    public string ExcelFilepath { get; set; } = "";
    /// <summary>
    /// 處理區分: "A", "U", "D" (均為大寫)
    /// A: 代表新增推播並依下述指定參數做(排程)推播,並回傳一個BatchNo ,用來當做修改及刪除時的鍵值
    /// U: 依指定的BatchNo 更新訊息(部份)內容,可更新欄位如下: Title , Content , MessageTypeCode , CategoryCode , ImageUrl , WebUrl , ShareTo , ExternalTitle , ExternalUrl , ExcelFilepath , Page2Title , Page3Title , Page3Content ,ListType , PlatformCode , Info01 , Info02 , Info03 , Info04 , Info05 , Info06 ,Info07 , Info08 , Info09 , Info10 , EndTime ! 若要更新其它部份(例如批次及排程時間...等),請先將此筆(批)推播刪除後重新新增!
    /// D: 刪除指定的BatchNo 訊息及排程(必需要尚未開始推播前呼叫,否則不會生效)
    /// </summary>
    public string PROCD { get; set; } = "A";
}

public sealed record TboxMessageData
{
    [JsonPropertyName("id")]
    public string? Id { get; init; }
    [JsonPropertyName("oneId")]
    public string? OneId { get; init; }
    [JsonPropertyName("title")]
    public string? Title { get; init; }
    [JsonPropertyName("body")]
    public string? Body { get; init; }
    [JsonPropertyName("epoch")]
    public long Epoch { get; init; }


}

public sealed record SentStatus
{
    [JsonPropertyName("sentResult")]
    public string SentResult { get; set; }

    [JsonPropertyName("sentTime")]
    public DateTime SentTime { get; set; }
}