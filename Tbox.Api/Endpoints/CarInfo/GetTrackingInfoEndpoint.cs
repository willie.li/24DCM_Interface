﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetTrackingInfoEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/tracking-info/{{vin}}/{{trackId:long}}", GetTrackingInfo)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetTrackingInfo))
            .WithDisplayName("查詢車輛異常移位內容")
            .WithSummary("實作 CMX06:查詢車輛異常移位內容")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            //.WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static IResult GetTrackingInfo()
    {
        return Results.NotFound();
    }
}