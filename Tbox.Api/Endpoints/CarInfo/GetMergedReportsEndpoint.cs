﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetMergedReportsEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/merged-lists/{{vin}}/{{epochstart:long}}/{{epochend:long}}", GetMergedReports)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetMergedReports))
            .WithDisplayName("整合查詢（正常異常/一般/借車）")
            .WithSummary("實作 CMX310456 整合查詢（正常異常/一般/借車）")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetMergedReports(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] long epochstart, [FromRoute] long epochend)
    {

        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.Now;

        epochstart = await dataAccessService.VehicleInfoB2s.EpochstartRedefine(validVIN, epochstart);

        List<CMX310456MergedModel> tracks = new();

        // call cmx03
        var cmx03Reports = await carInfoService.GetDriveReportList(validVIN, epochstart, epochend);
        await ProceedCMX030410Data(carInfoService, tracks, cmx03Reports, validVIN);

        // call cmx10
        var cmx10Reports = await carInfoService.GetLendingDriveReport(validVIN, epochstart, epochend);
        await ProceedCMX030410Data(carInfoService, tracks, cmx10Reports, validVIN, true);

        // call cmx05, cmx06
        await ProceedCMX0506Data(carInfoService, tracks, validVIN, epochstart, epochend);

        foreach (var t in tracks)
        {
            if (t.AbnormalResults?.Any() == true)
            {
                t.AbnormalResults = t.AbnormalResults.OrderByDescending(o => o.Epoch);
            }
            if (t.RegularResults?.Any() == true)
            {
                t.RegularResults = t.RegularResults.OrderByDescending(o => o.Epoch);
            }
        }
        var orderedTracks = tracks.OrderByDescending(x => x.TrackId).ToArray();

        return Results.Ok(new ResultContainer<IEnumerable<CMX310456MergedModel>>
        {
            Result = orderedTracks,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }

    static async Task ProceedCMX030410Data(ICarInfoService carInfoService, List<CMX310456MergedModel> tracks, DriveReportModel? reportData, string vin, bool isLendingMode = false)
    {
        if (reportData?.Reports?.Any() == true)
        {
            foreach (var row in reportData.Reports)
            {
                if (row is not null)
                {
                    await ProceedCMX030410DataLopper(carInfoService, tracks, row, vin, isLendingMode);
                }
            }
        }
    }

    static async Task ProceedCMX030410DataLopper(ICarInfoService carInfoService, List<CMX310456MergedModel> tracks, ReportDetailModel row, string vin, bool isLendingMode = false)
    {
        var data = new CMX310456MergedModel
        {
            IsLendingMode = isLendingMode,
            TrackId = row.TrackId,
            EpochStart = row.EpochStart,
            EpochEnd = row.EpochEnd,
            TravelDistance = row.TravelDistance,
            AvgFuelConsumption = row.AvgFuelConsumption / 10,
            EvPercentage = row.EvPercentage
        };

        data.TravelTime = Utilities.ComputeTravelTimeAsHM(data.EpochStart, data.EpochEnd);

        var speed = (double)data.TravelDistance / 10 / ((double)(data.EpochEnd - data.EpochStart) / 3600);
        data.AvgSpeed = (int)speed;

        // call cmx04 from each trackid
        var trackDetailsData = await carInfoService.GetDriveReportInfo(vin, row.TrackId);
        if (trackDetailsData?.Any() == true)
        {
            var trackDetails = trackDetailsData.Where(x => x.Lat > 0 && x.Lon > 0);
            if (trackDetails?.Any() == true)
            {
                foreach (var td in trackDetails)
                {
                    td.Lat /= 1000000; // 25.060274,
                    td.Lon /= 1000000; // 121.457728,
                }

                data.HasRegularResults = true;
                data.RegularResults = trackDetails;
            }
        }
        tracks.Add(data);
    }

    static async Task ProceedCMX0506Data(ICarInfoService carInfoService, List<CMX310456MergedModel> tracks, string vin, long epochstart, long epochend, bool isLendingMode = false)
    {
        var cmx05Reports = await carInfoService.GetTrackingReport(vin, epochstart, epochend);
        if (cmx05Reports?.Reports?.Any() == true)
        {
            foreach (var row in cmx05Reports.Reports)
            {
                if (row is not null)
                {
                    await ProceedCMX0506DataLooper(carInfoService, tracks, row, vin, isLendingMode);
                }
            }
        }
    }

    static async Task ProceedCMX0506DataLooper(ICarInfoService carInfoService, List<CMX310456MergedModel> tracks, TrackingData row, string vin, bool isLendingMode = false)
    {
        var data = new CMX310456MergedModel
        {
            IsLendingMode = isLendingMode,

            TrackId = row.TrackId,
            EpochStart = row.EpochStart,
            EpochEnd = row.EpochEnd,

            TravelDistance = -1,
            AvgFuelConsumption = -1,
            AvgSpeed = -1,
            EvPercentage = -1
        };

        data.TravelTime = Utilities.ComputeTravelTimeAsHM(data.EpochStart, data.EpochEnd);

        // call cmx06 from each trackid
        var cmx06Reports = await carInfoService.GetTrackingInfo(vin, row.TrackId);
        if (cmx06Reports?.Any() == true)
        {
            var trackDetails = cmx06Reports.Where(x => x.Lat > 0 && x.Lon > 0);
            if (trackDetails?.Any() == true)
            {
                foreach (var td in trackDetails)
                {
                    ProceedCMX0506DataDeepLooper(data, trackDetails, td);
                }
            }
            tracks.Add(data);
        }
    }

    static void ProceedCMX0506DataDeepLooper(CMX310456MergedModel data, IEnumerable<TrackingInfoModel> trackDetails, TrackingInfoModel td)
    {
        td.Lat /= 1000000; // 25.060274,
        td.Lon /= 1000000; // 121.457728,

        data.HasAbnormalResults = true;
        data.AbnormalResults = trackDetails; //.OrderBy(x => x.Epoch);

        // data.AbnormalResults has 2+
        var abnormalMovingDistance = 0d;
        if (data.AbnormalResults.Count() >= 2)
        {
            var first = data.AbnormalResults.First();
            var last = data.AbnormalResults.Last();
            if (first is not null && last is not null)
            {
                abnormalMovingDistance = Utilities.CalculateDistance(
                    ((double)first.Lat, (double)first.Lon),
                    ((double)last.Lat, (double)last.Lon)
                );
            }
        }
        data.AbnormalMovingDistance = abnormalMovingDistance;
    }
}