﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetDriveReportInfoEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/report-info/{{vin}}/{{trackId:long}}", GetDriveReportInfo)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetDriveReportInfo))
            .WithDisplayName("查詢車輛行駛報告內容")
            .WithSummary("實作 CMX04:查詢車輛行駛報告內容")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetDriveReportInfo(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] int trackId)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.Now;
        var ret = await carInfoService.GetDriveReportInfo(validVIN, trackId);

        return Results.Ok(new ResultContainer<IEnumerable<GpsInfoModel?>>
        {
            Result = ret,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }
}