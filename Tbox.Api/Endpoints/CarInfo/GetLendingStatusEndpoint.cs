﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetLendingStatusEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/lending-status/{{vin}}", GetLendingStatus)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetLendingStatus))
            .WithDisplayName("取得借車模式設定")
            .WithSummary("實作 CMX15:取得借車模式設定")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetLendingStatus(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var (status, result) = await carInfoService.GetLendingMode(validVIN);
        return status
            ? Results.Ok(result)
            : Results.BadRequest(result);
    }
}