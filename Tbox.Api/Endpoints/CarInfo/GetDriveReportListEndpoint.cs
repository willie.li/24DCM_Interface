﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetDriveReportListEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/report-list/{{vin}}/{{epochstart:long}}/{{epochend:long}}", GetDriveReportList)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetDriveReportList))
            .WithDisplayName("查詢車輛行駛報告列表")
            .WithSummary("實作 CMX03:查詢車輛行駛報告列表")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetDriveReportList(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] long epochstart, [FromRoute] long epochend)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.Now;

        var ret = await carInfoService.GetDriveReportList(validVIN, epochstart, epochend);

        return Results.Ok(new ResultContainer<DriveReportModel?>
        {
            Result = ret,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }
}