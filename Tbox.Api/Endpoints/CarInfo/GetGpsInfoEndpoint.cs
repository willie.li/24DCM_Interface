﻿namespace Tbox.Api.Endpoints.CarInfo;

public class GetGpsInfoEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/gps/{{vin}}", GetGpsInfo)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetGpsInfo))
            .WithDisplayName("查詢車輛GPS位置")
            .WithSummary("實作 CMX01:查詢車輛GPS位置")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetGpsInfo(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.Now;

        var ret = await carInfoService.GetCarGps(validVIN);

        return Results.Ok(new ResultContainer<GpsInfoModel?>
        {
            Result = ret,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }
}