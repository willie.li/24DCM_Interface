﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class EndLendingModeEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPost($"{Constants.CarInfoEndpointsPrefix}/end-lending/{{vin}}", EndLendingMode)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(EndLendingMode))
            .WithDisplayName("關閉借車模式")
            .WithSummary("實作 CMX12:關閉借車模式")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> EndLendingMode(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var (status, result) = await carInfoService.EndLendingMode(validVIN);
        return status
            ? Results.Ok()
            : Results.BadRequest(result);
    }
}