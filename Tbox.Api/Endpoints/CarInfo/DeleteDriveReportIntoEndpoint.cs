﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class DeleteDriveReportIntoEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapDelete($"{Constants.CarInfoEndpointsPrefix}/report-info/{{vin}}/{{trackId:long}}", DeleteDriveReportInfo)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(DeleteDriveReportInfo))
            .WithDisplayName("刪除車輛行駛報告內容")
            .WithSummary("實作 CMX04-2:刪除車輛行駛報告內容")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();

        app.MapPost($"{Constants.CarInfoEndpointsPrefix}/report-info/{{vin}}/{{trackId:long}}", DeleteDriveReportInfos)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(DeleteDriveReportInfos))
            .WithDisplayName("刪除車輛行駛報告內容（多筆）")
            .WithSummary("實作 CMX04-2:刪除車輛行駛報告內容（多筆）")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> DeleteDriveReportInfo(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] long trackId)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        await carInfoService.DeleteDriveReportInfo(validVIN, trackId);

        return Results.Ok();
    }

    static async Task<IResult> DeleteDriveReportInfos(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService, IEnumerable<DeletingRequestDataModel> data)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validOneId))
            return Results.Unauthorized();

        if (data?.Any() == true)
        {
            foreach (var q in data)
            {
                await carInfoService.DeleteDriveReportInfo(validVIN, q.TrackId);
            }
        }
        return Results.Ok();
    }
}