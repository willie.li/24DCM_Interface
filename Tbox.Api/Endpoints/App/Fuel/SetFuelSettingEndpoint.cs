﻿namespace Tbox.Api.Endpoints.App.Fuel;

public sealed class SetFuelSettingEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPut($"{Constants.AppEndpointsPrefix}/fuel/setting/{{vin}}/{{oneId}}/{{oilType:int}}", SetFuelSetting)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(SetFuelSetting))
            .WithDisplayName("設定油品")
            .WithSummary("設定油品")
            .WithDescription($"oil type：0 Oil92, 1 Oil95, 2 Oil98, 3 Diesel{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> SetFuelSetting(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] string oneId, [FromRoute] int oilType)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        await dataAccessService.FuelSettings.AddOrUpdate(validVIN, oneId, oilType);
        return Results.Ok();
    }
}