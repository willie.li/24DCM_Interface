﻿namespace Tbox.Api.Endpoints.App.Fuel;

public sealed class GetFuelSettingEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.AppEndpointsPrefix}/fuel/setting/{{vin}}/{{oneId}}", GetFuelSetting)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(GetFuelSetting))
            .WithDisplayName("取得油品設定")
            .WithSummary("取得油品設定")
            .WithDescription(Constants.REQUIRED_HEADERS_HINT)
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetFuelSetting(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] string oneId)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var setting = await dataAccessService.FuelSettings.GetSetting(validVIN, oneId);
        return Results.Ok(setting);
    }
}