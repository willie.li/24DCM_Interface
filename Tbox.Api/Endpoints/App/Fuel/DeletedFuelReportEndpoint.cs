﻿namespace Tbox.Api.Endpoints.App.Fuel;

public sealed class DeletedFuelReportEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapDelete($"{Constants.AppEndpointsPrefix}/fuel/{{vin}}/{{id}}", DeleteFuelReport)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(DeleteFuelReport))
            .WithDisplayName("刪除加油日記記錄")
            .WithSummary("刪除加油日記記錄")
            .WithDescription(Constants.REQUIRED_HEADERS_HINT)
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> DeleteFuelReport(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] string id)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        vin = validVIN;

        var r = await dataAccessService.FuelReports.Delete(id);
        return Results.Ok(r);
    }
}