﻿namespace Tbox.Api.Endpoints.App.Fuel;

public sealed class SyncPriceEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.AppEndpointsPrefix}/price-sync", SyncPrice)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(SyncPrice))
            .WithDisplayName("油價同步")
            .WithSummary("油價同步 only for webjob console")
            .WithDescription("")
            .WithOpenApi();
    }

    static async Task<IResult> SyncPrice([FromServices] IDataAccessService dataAccessService, [FromServices] ILogger<SyncPriceEndpoint> logger)
    {
        var (oil92, oil95, oil98, diesel) = await GetFuelPriceV2(logger);
        if (oil92 > 0 && oil95 > 0 && oil98 > 0 && diesel > 0)
        {
            await dataAccessService.FuelPrices.UpdatePrice(oil92, oil95, oil98, diesel);
            return Results.Ok();
        }
        return Results.NoContent();
    }

    static async Task<(double oil92, double oil95, double oil98, double diesel)> GetFuelPriceV2(ILogger<SyncPriceEndpoint> logger)
    {
        const string PRICE_PROVIDER_URL = "https://mochi-cloud-api-alpha.pklotcorp.com/v1/gas_pricing";

        var httpCliet = new HttpClient(new SocketsHttpHandler { });
        var req = new HttpRequestMessage
        {
            RequestUri = new Uri(PRICE_PROVIDER_URL),
            Method = HttpMethod.Get
        };
        try
        {
            var resp = await httpCliet.SendAsync(req);
            if (resp.IsSuccessStatusCode)
            {
                var content = await resp.Content.ReadAsStringAsync();
                var oilData = JsonSerializer.Deserialize<FuelDataV2>(content);
                var detail = oilData?.Data?.Current;
                if (detail is not null)
                {
                    _ = double.TryParse(detail.Gas92, out var oil92);
                    _ = double.TryParse(detail.Gas95, out var oil95);
                    _ = double.TryParse(detail.Gas98, out var oil98);
                    _ = double.TryParse(detail.Diesel, out var diesel);

                    return (oil92, oil95, oil98, diesel);
                }
            }
            return (-1, -1, -1, -1);
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            logger.LogError($"{nameof(SyncPrice)} | {nameof(GetFuelPriceV2)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return (-1, -1, -1, -1);
#endif
        }
    }
}