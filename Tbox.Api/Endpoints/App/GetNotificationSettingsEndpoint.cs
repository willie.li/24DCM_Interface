﻿namespace Tbox.Api.Endpoints.App;

public sealed class GetNotificationSettingsEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.AppEndpointsPrefix}/GetNotificationSettings/{{vin}}/{{oneId}}/{{dxid}}", GetNotificationSettings)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(GetNotificationSettings))
            .WithDisplayName("取得推播收取設定")
            .WithSummary("取得推播收取設定（依裝置不同，故須使用 Dxid）")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetNotificationSettings(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] string oneId, [FromRoute] string dxid)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var ret = await dataAccessService.TBoxNotificationSettings.GetSettings(validVIN, oneId, dxid);
        return Results.Ok(ret);
    }
}