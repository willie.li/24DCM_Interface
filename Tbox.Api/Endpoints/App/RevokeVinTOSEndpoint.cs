﻿namespace Tbox.Api.Endpoints.App;

public sealed class RevokeVinTOSEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPut($"{Constants.AppEndpointsPrefix}/RevokeVinTOS/{{vin}}/{{oneId}}", RevokeVinTOS)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(RevokeVinTOS))
            .WithDisplayName("徹銷同意狀態綁定")
            .WithSummary("徹銷同意狀態綁定")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> RevokeVinTOS(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromServices] IHttpContextAccessor httpContextAccessor,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] string oneId)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var ip = string.Empty;

#if RELEASE
        ip = Strings.TryGetClientIP(httpContextAccessor);
#endif

        var data = await dataAccessService.VehicleInfoB2s.RevokeVinTOS(validVIN, oneId, ip);
        return Results.Ok(data);
    }
}