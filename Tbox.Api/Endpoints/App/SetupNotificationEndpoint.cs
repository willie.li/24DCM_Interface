﻿namespace Tbox.Api.Endpoints.App;

public sealed class SetupNotificationEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPut($"{Constants.AppEndpointsPrefix}/SetupNotification", SetupNotification)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(SetupNotification))
            .WithDisplayName("設定車況推播開關")
            .WithSummary("App 設定車況推播收取開關")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> SetupNotification(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromBody] TBoxNotificationSettingRequestData data)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        if (data is null)
        {
            return Results.BadRequest("missing parameter");
        }

        data.VIN = validVIN;
        //data.OneId = validOneId;
        //data.Dxid = validOneId;

        await dataAccessService.TBoxNotificationSettings.AddOrUpdate(data);

        return Results.Ok();
    }
}