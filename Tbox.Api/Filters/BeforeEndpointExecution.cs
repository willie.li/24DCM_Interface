﻿namespace Tbox.Api.Filters;

public sealed class BeforeEndpointExecution : IEndpointFilter
{
    private readonly ILogger<BeforeEndpointExecution> _logger;
    private readonly IEncryptedAppSettings<AppSettings> _appSettings;

    public BeforeEndpointExecution(ILogger<BeforeEndpointExecution> logger, IEncryptedAppSettings<AppSettings> appSettings)
    {
        _logger = logger;
        _appSettings = appSettings;
    }

    public ValueTask<object?> InvokeAsync(EndpointFilterInvocationContext context, EndpointFilterDelegate next)
    {
        // things todo:

        return next.Invoke(context);
    }
}