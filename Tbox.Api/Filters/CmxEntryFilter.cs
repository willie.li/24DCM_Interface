﻿namespace Tbox.Api.Filters;

public sealed class CmxEntryFilter : IEndpointFilter
{
    private readonly ILogger<BeforeEndpointExecution> _logger;
    private readonly IEncryptedAppSettings<AppSettings> _appSettings;

    public CmxEntryFilter(ILogger<BeforeEndpointExecution> logger, IEncryptedAppSettings<AppSettings> appSettings)
    {
        _logger = logger;
        _appSettings = appSettings;
    }

    public async ValueTask<object?> InvokeAsync(EndpointFilterInvocationContext context, EndpointFilterDelegate next)
    {
        var entry = context.HttpContext.Request.Host.Host;
        if (!entry.Equals(_appSettings.AppSettings.CmxEntryDomain, StringComparison.OrdinalIgnoreCase))
        {
            if (Strings.IsLocalhost(entry) || entry.Equals(_appSettings.AppSettings.DevEntryDomain, StringComparison.OrdinalIgnoreCase))
            {
                return await next(context);
            }
            _logger.LogWarning($"{nameof(CmxEntryFilter)} | {nameof(InvokeAsync)} | ${entry} is not an CmxEntryDomain.");
            return Results.Content("{0} is not an CmxEntryDomain.", entry);
        }
        return await next(context);
    }
}